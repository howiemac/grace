# grace-package contents

### "grace" binary

    $ grace start
    $ grace stop
    $ grace restart
    $ grace create <appname>

### GRACE app

- a default app and database (both named "grace")

### app generation

- app: prototype application
- create_app: script to create an app

### app support

- evo: default system-wide templates
- site: flat file webserver resources

### GRACE library routines

- lib: library routines, including data types
- data: database interface
- render: .evo html templating
- serve: application server

### GRACE classes ("klasses") for use in apps

- foundation klass
  - Page: page hierarchy, including image and file handling
- (in)security klass
  - User:
  (GRACE expects a trusted environment but provides hooks for external security)

### system configuration

 - config_base.py: system-wide defaults
 - config.py: app config
 - config_site.py: overrides for this server / app
