(see serve.app and elsewhere)

### Config items (per evoke => per grace):

app_filepath => app_filepath
app_fullpath => app_filepath   (note: app_fullpath is used in evoke's nevo.py and render/html.py)
evoke_filepath => grace_filepath
app => appname

### replaced

MySQL/MariaDB => SQLite
Twisted => SimpleServer

### major changes

data.DB, data.schema : rewritten for SQLite
serve : rejigged to include SimpleServer, and custom handing of POST arguments (both quoted and multiform)
- serve.app: multiserver removed, plus minor adjustments

### removed

multi-server
Vars
Users (and Sessions etc) - meaning no permissions system...

### minor changes

- data.data : %% for mysql => ? for SQLite (much neater!). No database specified for tables. API unchanged.
- data.patch : not implemented (will be if / when required)
- req.request => req._v_request
- lib.<TYPES> : DATE adjusted for SQLite (should still work with MySQL...). _v_SQLite_type added for each TYPE
- Page - minor adjustments
- restructuring of file-tree, aiming to eliminate symlinks and the need for local config (not quite achieved yet)

### added

- Grace database (ie Grace itself is an app!) : intended for smooth startup, and for presenting documentation and a more friendly user interface
- local backlinks (backlinks.py)
