**Evoke** is a simple and powerful python web application server with custom display types and pythonic "evo" templating.

Evoke enables easy creation of hierarchical linked web-page text content, with image and file upload/download.

Evoke is multi-user, and is used via a browser over the internet, or on intranets, or stand-alone (localhost).

The Evoke module allows you to create Evoke apps, These are web-server-applications which, via the Evoke API:
- use Twisted webserver (optionally proxied via Apache) to serve the data
- use MySQL for indexed data storage, with the data being presented to you as python objects
- produce HTML output via Evoke's own "evo" templating, and the Evoke display classes, by default using Bootstrap4 for CSS, and JQuery for Javascript

Evoke is a longstanding and stable system, which has been in continual use for commercial mission-critical systems since its inception in 2001.

#### versions

The current stable production version of Evoke is [https://gitlab.com/howiemac/evoke6 evoke 6], for python3.

For the current development version, see [evoke 7](https://gitlab.com/howiemac/evoke7).

For the historic stable python 2 version see [evoke 4](https://gitlab.com/howiemac/evoke4).

#### requirements

- python3 (tested on 3.6.2)
- linux or MacOS (should also work on BSD - but not yet tested)
- mysql / mariadb

#### installation

    pip3 install evoke