GRACE uses [https://daringfireball.net/projects/markdown/ Markdown], by John Gruber, for text input and display when the TEXT type is used. TEXT is the default for page contents. This page is written in markdown.

Markdown is a text-to-HTML conversion tool for writers. It allows you to easily create web (HTML) pages which are visually appealing, using an easy-to-read, easy-to-write plain text format. By following a few simple rules, you can create a variety of heading styles, links, numbered or bulleted lists, italics and bold text styles, etc. 

The idea is that a Markdown-formatted document should be publishable as-is, as plain text which is easily readable, and which doesn't look like computer code

That said, you can also use HTML tags mixed in with the markdown.

See:    

- [https://daringfireball.net/projects/markdown/ markdown introduction]  
- [https://daringfireball.net/projects/markdown/basics markdown basics]  
- [https://daringfireball.net/projects/markdown/syntax markdown syntax]  

GRACE uses The Python Markdown Project's python implementation of markdown, with a couple of enhancements:  

- a simpler link style - an alternative to Markdown's \[caption\](link) syntax, which also works here. Note that GRACE uids can be used in place of urls, even when using the Markdown syntax:  
    
        [url-or-uid caption]  

- inline images (displays a thumbnail of the linked image inline):

        [uid caption]

Markdown is used for the text. Images (other than inline images) and file attachments are handled outwith Markdown.