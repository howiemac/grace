# SQLite python interface

https://www.pythoncentral.io/introduction-to-sqlite-in-python/
https://www.pythoncentral.io/advanced-sqlite-usage-in-python/
see also https://likegeeks.com/python-sqlite3-tutorial/

## relation between Python datatypes and SQLite datatypes:

    None type is converted to NULL
    int type is converted to INTEGER
    float type is converted to REAL
    str type is converted to TEXT
    bytes type is converted to BLOB

## connecting

import sqlite3

> We use the function sqlite3.connect to connect to the database. 
> We can use the argument ":memory:" to create a temporary DB in the RAM or pass the name of a file to open or create it.
	
### create a database in RAM

db = sqlite3.connect(':memory:')

### create or open a file called mydb with a SQLite3 DB

db = sqlite3.connect('data/mydb')

> When we are done working with the DB we need to close the connection:

db.close()


## CREATE a table

For any operation with the database, we need to get a cursor object, and pass the SQL statements to the cursor object. 
Finally, commit the changes. Note that the commit function is invoked on the db object, not the cursor object. 


cursor = db.cursor()
cursor.execute('''
    CREATE TABLE users(id INTEGER PRIMARY KEY, name TEXT,
                       phone TEXT, email TEXT unique, password TEXT)
''')
db.commit()


## DROP a table
	
cursor = db.cursor()
cursor.execute('''DROP TABLE users''')
db.commit()


## INSERT data

If you need values from Python variables it is recommended to use the "?" placeholder. 
Never use string operations or concatenation to make your queries because is very insecure. 

cursor.execute('''INSERT INTO users(name, phone, email, password)
                  VALUES(?,?,?,?)''', (name1,phone1, email1, password1)) 
db.commit()

#### Another way to do this is passing a dictionary using the ":keyname" placeholder:
	
cursor.execute('''INSERT INTO users(name, phone, email, password)
                  VALUES(:name,:phone, :email, :password)''',
                  {'name':name1, 'phone':phone1, 'email':email1, 'password':password1})

#### If you need to insert several users use executemany and a list with the tuples:
	
users = [(name1,phone1, email1, password1),
         (name2,phone2, email2, password2),
         (name3,phone3, email3, password3)]
cursor.executemany(''' INSERT INTO users(name, phone, email, password) VALUES(?,?,?,?)''', users)
db.commit()

#### If you need to get the id of the row you just inserted use lastrowid:
	
id = cursor.lastrowid
print('Last row id: %d' % id)


## SELECT data

#### Use fetchone() to retrieve a single row, or fetchall() to retrieve all the rows.
	
cursor.execute('''SELECT name, email, phone FROM users''')
user1 = cursor.fetchone() #retrieve the first row
print(user1[0]) #Print the first column retrieved(user's name)
all_rows = cursor.fetchall()
for row in all_rows:
    # row[0] returns the first column in the query (name), row[1] returns email column.
    print('{0} : {1}, {2}'.format(row[0], row[1], row[2]))

#### The cursor object works as an iterator, invoking fetchall() automatically:
	
cursor.execute('''SELECT name, email, phone FROM users''')
for row in cursor:
    # row[0] returns the first column in the query (name), row[1] returns email column.
    print('{0} : {1}, {2}'.format(row[0], row[1], row[2]))


#### To retrive data with conditions, use again the "?" placeholder:

user_id = 3
cursor.execute('''SELECT name, email, phone FROM users WHERE id=?''', (user_id,))
user = cursor.fetchone()


#### access the columns of a query by name instead of by index:

> use row_factory class sqlite3.Row
	
db = sqlite3.connect('data/mydb')
db.row_factory = sqlite3.Row
cursor = db.cursor()
cursor.execute('''SELECT name, email, phone FROM users''')
for row in cursor:
    # row['name'] returns the name column in the query, row['email'] returns email column.
    print('{0} : {1}, {2}'.format(row['name'], row['email'], row['phone']))
db.close()



## UPDATE 

newphone = '3113093164'
userid = 1
cursor.execute('''UPDATE users SET phone = ? WHERE id = ? ''', (newphone, userid))
db.commit()

## DELETE

delete_userid = 2
cursor.execute('''DELETE FROM users WHERE id = ? ''', (delete_userid,))
db.commit()


## rollback any change to the database since the last call to commit

cursor.execute('''UPDATE users SET phone = ? WHERE id = ? ''',
(newphone, userid))
db.rollback() # The user's phone is not updated


## Exceptions 

All exceptions are the instances of the class derived from the BaseException.

SQLite3 has the following main Python exceptions:
- DatabaseError
- IntegrityError - a subclass of DatabaseError, raised when there is a data integrity issue 
    eg foreign data isn’t updated in all tables resulting in the inconsistency of the data
- ProgrammingError - eg syntax errors, table not found, function called with the wrong number of parameters/ arguments
- OperationalError - non-programmer error, eg unusual disconnection
- NotSupportedError - eg method not defined or supported


### best practice is to use a surrounding try clause:
	
import sqlite3 #Import the SQLite3 module
try:
    # Creates or opens a file called mydb with a SQLite3 DB
    db = sqlite3.connect('data/mydb')
    # Get a cursor object
    cursor = db.cursor()
    # Check if table users does not exist and create it
    cursor.execute('''CREATE TABLE IF NOT EXISTS
                      users(id INTEGER PRIMARY KEY, name TEXT, phone TEXT, email TEXT unique, password TEXT)''')
    # Commit the change
    db.commit()
# Catch the exception - "catch-all"
except Exception as e:
    # Roll back any change if something goes wrong
    db.rollback()
    raise e
finally:
    # Close the db connection
    db.close()

### We can use the connection object (db here) to automatically commit or rollback transactions:
	 
> note that we call execute on the db object, not the cursor object.

try:
    with db:
        db.execute('''INSERT INTO users(name, phone, email, password)
                  VALUES(?,?,?,?)''', (name1,phone1, email1, password1))
except sqlite3.IntegrityError:
    print('Record already exists')
finally:
    db.close()

> If the insert statement raises an exception, the transaction will be rolled back and the message gets printed
> Otherwise the transaction will be committed. 

### simple connection function 

from sqlite3 import Error

def sql_connection():
    try:
        db = sqlite3.connect('data/mydb')
        print("Connection is established.")
    except Error:
        print(Error)
    finally:
        db.close()


## date and datetime

### can insert a date object into the database - it returns this as a string:

> note that there is DATE column type, (provided by the adaptor?)

>>> import sqlite3
>>> from datetime import date, datetime
>>>
>>> db = sqlite3.connect(':memory:')
>>> c = db.cursor()
>>> c.execute('''CREATE TABLE example(id INTEGER PRIMARY KEY, created_at DATE)''')
>>>
>>> # Insert a date object into the database
>>> today = date.today()
>>> c.execute('''INSERT INTO example(created_at) VALUES(?)''', (today,))
>>> db.commit()
>>>
>>> # Retrieve the inserted object
>>> c.execute('''SELECT created_at FROM example''')
>>> row = c.fetchone()
>>> print('The date is {0} and the datatype is {1}'.format(row[0], type(row[0])))
# The date is 2013-04-14 and the datatype is <class 'str'>
>>> db.close()

### to return a date object: 

...
>>> db = sqlite3.connect(':memory:', detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
...
# The date is 2013-04-14 and the datatype is <class 'datetime.date'>
...

### for datetime, use timestamp column type (provided by the adaptor?): 
	
>>> c.execute('''CREATE TABLE example(id INTEGER PRIMARY KEY, created_at timestamp)''')
>>> # Insert a datetime object
>>> now = datetime.now()
>>> c.execute('''INSERT INTO example(created_at) VALUES(?)''', (now,))
>>> db.commit()
>>>
>>> # Retrieve the inserted object
>>> c.execute('''SELECT created_at FROM example''')
>>> row = c.fetchone()
>>> print('The date is {0} and the datatype is {1}'.format(row[0], type(row[0])))
# The date is 2013-04-14 16:29:11.666274 and the datatype is <class 'datetime.datetime'>

### to store and retrieve a datetime to/from a date column:

>>> c.execute('''CREATE TABLE example(id INTEGER PRIMARY KEY, created_at DATE)''')
>>> # We are going to insert a datetime object into a DATE column
>>> now = datetime.now()
>>> c.execute('''INSERT INTO example(created_at) VALUES(?)''', (now,))
>>> db.commit()
>>> 
>>> # Retrieve the inserted object
>>> c.execute('''SELECT created_at as "created_at [timestamp]" FROM example''')
(etc)

ie: 'as "created_at [timestamp]"' makes the adapter parse the object correctly


## Insert multiple rows with SQLite's executemany

import sqlite3
db = sqlite3.connect(':memory:')
c = db.cursor()
c.execute('''CREATE TABLE users(id INTEGER PRIMARY KEY, name TEXT, phone TEXT)''')
users = [
    ('John', '5557241'), 
    ('Adam', '5547874'), 
    ('Jack', '5484522'), 
    ('Monthy',' 6656565')
]
c.executemany('''INSERT INTO users(name, phone) VALUES(?,?)''', users)
db.commit()
 
# Print the users
c.execute('''SELECT * FROM users''')
for row in c:
    print(row)
 
db.close()


## Execute SQL File with SQLite's executescript

execute() only allows you to execute a single SQL command. 
executescript() executes a list of SQL commands

script = '''CREATE TABLE users(id INTEGER PRIMARY KEY, name TEXT, phone TEXT);
            CREATE TABLE accounts(id INTEGER PRIMARY KEY, description TEXT);
            
            INSERT INTO users(name, phone) VALUES ('John', '5557241'), ('Adam', '5547874'), ('Jack', '5484522');
'''
c.executescript(script)
 
# Print the results
c.execute('''SELECT * FROM users''')
for row in c:
    print(row)
 
db.close()

### to read the script from a file:

fd = open('myscript.sql', 'r')
script = fd.read()
c.executescript(script)
fd.close()


## defining SQLite SQL Functions

eg: to store passwords in the database, which need to be encrypted:

import sqlite3 #Import the SQLite3 module
import hashlib

def encrypt_password(password):
    # Do not use this algorithm in a real environment
    encrypted_pass = hashlib.sha1(password.encode('utf-8')).hexdigest()
    return encrypted_pass
 
db = sqlite3.connect(':memory:')

# Register the function
db.create_function('encrypt', 1, encrypt_password)

c = db.cursor()
c.execute('''CREATE TABLE users(id INTEGER PRIMARY KEY, email TEXT, password TEXT)''')
user = ('johndoe@example.com', '12345678')
c.execute('''INSERT INTO users(email, password) VALUES (?,encrypt(?))''', user)


## The create_function takes 3 parameters: 

- name (used to call the function)
- number of parameters expected 
- a callable object (the function itself).
