## backlinks

Backlinks are now maintained automatically for local content - i.e. any link with a uid:

        [3]

looks like this: [3], and you will find a backlink at the foot of that page linking to this page. 