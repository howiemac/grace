Grace is a single-user app-server for use in a trusted environment (i.e. localhost), via a browser. 

Grace enables easy creation of hierarchical linked text content, with image and file upload/download.

Grace is written in python3, the [8] pythonic "evo" templating language, SQL, Bootstrap, and JQuery. 

Grace utilises [5 Markdown] for simple text richness, and SQLite for storage, and the python Simple HTTP Server. 

Grace can be extended, and Grace apps can be written, using the Grace Python API. Data is handled and displayed via Grace's custom display classes.

Note that Grace isn't suited for internet deployment. For this use [8], from which Grace is derived. 

## grace-package contents:

#### grace daemon

    $ grace start
    $ grace stop
    $ grace restart
    $ grace create <appname>

#### grace app

- a default app and database (named "grace" and "grace.db")
- site/ : flat file resources common to all apps
- site/data/ : image and other files associated with pages

#### app support

- app creation via grace daemon
- grace.lib: library routines, including display-oriented data types
- grace.data: database interface
- grace.render: .evo HTML templating
- grace.serve: application server
- grace.Page: foundation "klass" giving a page hierarchy, including images, and file uploads/downloads.

#### system configuration

 - config_base.py: system-wide defaults
 - config.py: app config
 - config_site.py: overrides for this server / app

## coming soon...

Grace is currently a work in progress. Planned future enhancements include:  

- an improved klass hierarchy
- multiple page hierarchies within one node 
- JSON export and import of any page hierarchy
- link-pages (like symbolic links) - allowing association of any page to a different parent
- image collection
- multimedia players (probably using VLC)
    - audio
    - video
- distributed interaction between Grace nodes
    - the basic policy proposed (draft status!) is:
        - every node is single-user, in a trusted environment (ie localhost)
        - data is shared read-only (or is not shared at all, as the case may be)
        - each node will store chosen data from other nodes
        - distributed sharing of file exports (JSON) via bittorrent
        - local storage of data shared by others (bittorrent)
- file consolidation: 
    - configuration to be moved to grace.db (or <appname>.db for created apps) 
    - flat file resources to be moved to data folder
- image and document file library interface, allowing tagging, selection and viewing
- audio and video files - tagging, selection and playing