"""
grace definition class for Data objects

IHM March 2020

schema syntax:

class Widget:
  table='widgets'             #optional table name - will default to the class name (lowercased).
  name=TAG                    #first attribute / column name....
  number=INT,100,KEY          #optional default . KEY will generate an index on this field
  date=DATE,KEY,'20000101'    #default and KEY can be swapped, but TYPE must come first
  comment=STR,KEY             #KEY on a STR or TEXT field will generate a text index (ie FULLTEXT) ***** TO BE IMPLEMENTED ************
  code=TAG,KEY                #KEY on a TAG will generate a normal index (unlike STR, which is otherwise equivalent)
  ... etc                     #as many as you like
  insert=[dict(name='whatever',number=123),dict(name="something",number=456)] #seed data (columns) for the table

The above definition implies (and requires) that there is a class called Widget in a module called Widget.py in the app code directory, or in the grace directory.

A schema class can be subclassed, eg to give a different class which uses the same database table (or indeed a different table with the same schema, or a modified version of it)

Note that column names can even be SQL keywords, as they are always `quoted`.
"""

from grace.lib import TAG, STR, CHAR, TEXT, INT, SMALLINT, TINYINT, FLOAT, DATE, FLAG, MONEY, TIME, REL, BLOB, sql_list, Error
from .DB import connect, execute, tables, columns, keys

KEY = 'KEY'
now = DATE().sql(quoted=False) #convenient shorthand for initialising dates

class Schema(object):
    """
  each instance requires:
  
  table='tablename'
  `fieldname`=TYPE,default,KEY' # for each field, where TYPE in TYPES , KEY and default are optional, and can be swapped in  order
  """
    TYPES = (TAG, STR, CHAR, TEXT, INT, SMALLINT, TINYINT, FLOAT, DATE, FLAG,
             MONEY, TIME, REL, BLOB)
    _v_built = []

    ################################ database maintenance ##########################

    @classmethod
    def build_database(self, database):
        " create or add defined tables to given sqlite3 database"
        #print('building database for ',self)
        self.table=getattr(self,'table',self.__name__.lower())  # NOTE: commented out in evoke 6, but why????
        self.database = database
        # get list of tables
        tablelist=tables()
        #print("DB TABLES:",tablelist)
        # get columns and indices for use by data.py
#        self._v_columns, self._v_keys, self._v_textkeys = self.get_columns_and_indices()
        self._v_columns, self._v_keys = self.get_columns_and_indices()
        self._v_schema = dict([('uid', INT)] + [
            (k, v[0]) for (k, v) in list(self._v_columns.items())
        ])
        # create the table or update the table  - unless there are no columns
        if self._v_columns:
            if self.table in tablelist:
                self.update_table()
            # if the table has not already been created (by a parent class), then create it
            elif f"{self.table}" not in self._v_built:
                self.create_table()
            self._v_built.append(f"{self.table}")
        #print('v_built=',database,self._v_built)

    @classmethod
    def get_columns_and_indices(self):
        ""
        columns = {}
        indices = []
        textindices = []
        for k in dir(self):
            v = getattr(self, k, None)
            if not v is self.TYPES:
                if not isinstance(v, tuple):
                    v = [v]
                else:  #v is a tuple...
                    v = list(v)
                if v[0] in self.TYPES:
                    if 'KEY' in v:  #we need an index
# text KEY not currently implemented
#                        if v[0]._v_mysql_type == "mediumtext":
#                            textindices.append(k)
#                        else:
                        indices.append(k)
                        v.remove('KEY')
                    if len(v) == 1:  #we have no default
                        v.append(v[0]._v_default)  #use the grace-type default
                    columns[k] = v
        #print(self.table,' columns: ',columns, '  indices: ',indices)
        return columns, indices #, textindices

    @classmethod
    def update_table(self):
        """
    FOR SAFETY REASONS WE WON'T DELETE OR MODIFY ANY DATA, except keys
    - add any new columns and keys to the table
    - drop any keys which are no longer defined in the schema
    - generate a warning if any db columns are no longer defined in the schema
    NOTE - we don't care about mismatches in column types, because sqlite will cope with this
         - self.insert changes are IGNORED
         - changes to defaults are ignored (O/S - fix this)
         - FULLTEXT KEYS HAVE YET TO BE IMPLEMENTED 
    """
        #print('columns = ',self._v_columns)
        #print('keys = ',self._v_keys)
        columnnames = list(self._v_columns.keys())
        tablecols = columns(self.table)
        tablekeys = keys(self.table)
        #print('columnames = ',columnnames)
        #print('db columns = ',tablecols)
        #print('db keys = ',tablekeys)
        # note: full-text-indices are not yet supported...
        #textkeys = self._v_textkeys
        #tabletextkeys = []
        for name in tablecols:
            if name != 'uid':
                if name not in columnnames:  #don't delete the column - safety first....
                    print(f"WARNING: column `{name}` in table `{self.table}` is not defined in schema")
        for name in columnnames:
            if name not in tablecols:
                v = self._v_columns[name]
                default =  'NULL' if v[1] is None else f"'{v[1]}'"
                sql=f"ALTER TABLE `{self.table}` ADD COLUMN `{name}` {v[0]._v_sqlite_type} DEFAULT {default}"
                print(sql)
                execute(sql)
        for key in self._v_keys:
            dbkey = f"{self.table}_{key}"
            if dbkey not in tablekeys:
                sql=f"CREATE INDEX `{dbkey}` ON {self.table}(`{key}`)"
                print(sql)
                execute(sql)
        for dbkey in tablekeys:
            key = dbkey[len(self.table)+1:]
            if key not in self._v_keys:
                sql=f"DROP INDEX `{dbkey}`"
                print(sql)
                execute(sql)
#        for i in textkeys:
#            if i not in tabletextkeys:
#                sql.append("add FULLTEXT (`%s`)" % i)
#        for i in tabletextkeys:
#            if i not in textkeys:
#                sql.append("drop KEY `%s`" % i)


    @classmethod
    def create_table(self):
        """
        - generate SQL code to create the table for this schema (ignoring any invalidly-specified attributes)
        - seed it with initial row inserts (from the config files)
        """
        # columns
        sql = f"CREATE TABLE {self.table}(\n`uid` INTEGER PRIMARY KEY"
        for (k, v) in list(self._v_columns.items()):
          default = 'NULL' if v[1] is None else f"'{v[1]}'"
          sql += f",\n`{k}` {v[0]._v_sqlite_type} DEFAULT {default}"
        sql += ")"
        print(sql)
        execute(sql)

        # indices
        for key in self._v_keys:
            dbkey = f"{self.table}_{key}"
            sql=f"CREATE INDEX `{dbkey}` ON `{self.table}`(`{key}`)"
            print(sql)
            execute(sql)

        # fulltext index not yet implemented............

        # seed data (from the config files)
        if hasattr(self, 'insert'):
            if isinstance(self.insert, type({})):  #allow for single item
                self.insert = [self.insert]  #convert to list
            sql=""
            for row in self.insert:
                sql += "INSERT INTO %s %s VALUES %s;" % (
                    self.table,
                    str(sql_list(list(row.keys()))).replace("'", "`"),
                    sql_list(list(row.values())))
            for i in sql.split(';'):
                if i:
                    print(i)
                    execute(i)
#        if __name__ == '__main__':
#            return sql
