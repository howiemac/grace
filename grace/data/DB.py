"""
The GRACE SQLite database interface. 

We connect to the database on startup, and maintain the one persistent
connection throughout.

For INSERT statements we return the new row uid.

For SELECT queries we return a list of row dictionaries.
  (a la MySQLdb python interface)

No default database - we set the database explicitly with the table.

Ian Howie Mackenzie (March 2020)
"""
import sqlite3, atexit


class DB(object):
    "SQLite database access"

    def connect(self, database):
        "connect to SQLite database (creating it if it doesn't exist)"

        def dict_factory(cursor, row):
            d = {}
            for idx, col in enumerate(cursor.description):
                d[col[0]] = row[idx]
            return d

        self.connection = sqlite3.connect(database)
        # configure SQLite module to return results as a list of dictionaries
        self.connection.row_factory = dict_factory
        # force autocommit as default
        self.connection.isolation_level=None

    def execute(self, sql, args=()):
        "perform a query safely"
#        print('execute: sql=',sql,'  args=',args)
        cursor=self.connection.cursor()
        cursor.execute(sql, args)
        if "INSERT" in sql.upper():
            res = cursor.lastrowid
        else: # return the result set
            res = cursor.fetchall()
        # commit
        self.connection.commit()
        cursor.close()
        return res

    def tables(self):
        "return a list of tables in the database"
        res=execute("select distinct tbl_name from sqlite_master")
        return [row["tbl_name"] for row in res]

    def columns(self,table):
        "return a list of columns for a given table"
        res=execute(f"select name from pragma_table_info('{table}')")
        return [row["name"] for row in res]
     
    def keys(self,table):
        "return a list of keys (indices) for a given table"
        res=execute(f"select name from pragma_index_list('{table}')")
        return [row["name"] for row in res]

# DB is a singleton object
db = DB()
connect=db.connect
execute=db.execute
tables=db.tables
columns=db.columns
keys=db.keys

# close the connection on shutdown
def close(db):
    "close the connection"
    try:
      db.connection.close()
    except:
      print("unable to close the db connection") 

atexit.register(close,db)
