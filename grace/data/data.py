"""
grace generic database interface:

(Christopher J Hurst 2003 onwards)
(modified Ian Howie Mackenzie Nov 2005 onwards)
"""

from .schema import *
from time import time
from pickle import loads, dumps

class DataObject(object):
    "Basic Model of a Persistent Data Object"
    __implements__ = 'DataObjectInterface'  # apart from the class methods

    def __init__(self, fields):
        "fields - [track changes for these names]"
        self._v_changed = {}
        self._v_fields = fields

    def __setattr__(self, name, value):
        "keep track of changed atts"
        object.__setattr__(self, name, value)
        if '_v_' not in name and name in self._v_fields:
            self._v_changed[name] = True

    def __delattr__(self, name):
        "keep track of changed atts"
        object.__delattr__(self, name)
        if name in self._v_changed:
            del self._v_changed[name]

    def update(self, args):
        "Update Object Fields"
        x = [
            setattr(self, k, v) for k, v in list(args.items())
            if k in self._v_fields
        ]

    def flush(self, only=[]):
        "return dictionary of changes, then reset self._v_changed"
        #print "flush2"
        res = {}
        changed = self._v_changed
        # filter by only if present
        if only:
            #      print ">>>>>>>>>>>>>> flushing only ",only
            keys = [i for i in list(changed.keys()) if i in only]
            d = {}
            for k in keys:
                d[k] = changed[k]
            changed = d
        # set object atts
        for i in changed:
            res[i] = getattr(self, i)
        self._v_changed = {}
        return res


class SQLDataObject(DataObject):
    "database-backed persistence"
    _v_schema = {}
    _v_fields = []

    # Initialisation
    def __init__(self):
        self._v_changed = {}

    def __setattr__(self, name, value):
        "for object attributes, create a new instance with the new value, thus converting it as required"
        if name in self._v_schema:
            #      print "CLASS",self._v_schema[name].__name__
            #      print "SET====",name,value
            value = self._v_schema[name](
                value
            )  #create an instance of the relevant type, thus processing value where required
#      print "  TO:",value
        DataObject.__setattr__(self, name, value)

    def __getattribute__(self, name):
        ""
        value = object.__getattribute__(self, name)
        try:
            _schematype = object.__getattribute__(self, '_v_schema')[name]

            if _schematype and type(value) != _schematype:
                value = _schematype(value)
                DataObject.__setattr__(self, name, value)
        except KeyError:
            pass

        if value and isinstance(value, REL):
            value = self.__class__.__dict__[name.capitalize()].get(value)
            object.__setattr__(
                self, name, value
            )  #bypass DataObject here, as we are not changing anything
        return value

#    def sql(self):
#        "for use by REL attributes that have been substituted (by __getattribute__ above) with grace objects (which have RemoteDataObject as a mix-in)"
#        return self.uid

    def flush(self, only=[]):
        "save changes to db, filtered by only"
        changes = DataObject.flush(self, only=only)
        if not changes:
            return
#    print ">>>> changes >>>>>",changes.items(),
#    for (k,v) in changes.items():
#      print ">>>> change >>>>>",k,v,type(v),repr(v),str(v)

        for k in changes:
            # handle REL objects
            if isinstance(changes[k], REL):
                changes[k] = int(changes[k])
            # uids
            if hasattr(changes[k], 'uid'):
                changes[k] = changes[k].uid
            # and DATE objects
            if isinstance(changes[k], DATE):
                changes[k] = changes[k].sql(quoted=False)

        # fix order of changes dict by converting to a list of tuples
        items = list(changes.items())
        # list of fields ready for SQL parameters
        fields = ', '.join(("`%s`=?" % k) for k, v in items)
        # self.uid always last in the list
        values = tuple([v for k, v in items] + [self.uid])
        # execute the SQL to store the changes
        sql = "update %s set %s where uid=?" % (self.table, fields)
        #    print ">>>> SQL >>>>>",r"%s" % sql
        #print sql
        execute(sql, values)

#    def quoted_pairs(self, items, op='='):
#        "quote key,value pairs according to _v_schema "
#        fields = []
#        for k, v in items:
#            if not isinstance(v, (Schema, DataObject)):
#                v = self._v_schema[k](v)
#            fields.append("`%s` %s %s" % (k, op, v.sql()))
#        return ", ".join(fields)

    @classmethod
    def sql_quoted_pairs(self, items, op='=', link=', '):
        "return sql and list of args suitable for SQLite arg substitution"
        fields = []
        sqlargs = []
        for k, v in items:
            if not isinstance(v, (Schema, DataObject)):
                v = self._v_schema[k](v)
            fields.append("`%s` %s ?" % (k, op))
            sqlargs.append(v.sql(quoted=False))
        sql = f" {link} ".join(fields)
        return sql, sqlargs

# Error Classes for MassProducedSQLDataObject
class RecordNotFoundError(Error):
    "%s with uid=%d not found"


class RecordInUseError(Error):
    "%s with uid=%d is still in use"


class ListParameterConflictError(Error):
    "You shouldn't call list with a non-default what parameter when asObjects is true."


class InvalidOrderFieldError(Error):
    "Field in ORDER BY clause not found for this object."


class InvalidOrderDirectionError(Error):
    "Direction in ORDER BY clause not valid."


class Subscriptable(type):
    """metaclass to allow a class to be subscripted"""

    def __getitem__(cls, x):
        return cls.get(x)


class MassProducedSQLDataObject(SQLDataObject, metaclass=Subscriptable):
    "SQL data object for use with makeDataClass"

    @classmethod
    def exists(cls, uid):
        "return true if a record with this uid exists"
        sql = 'select * from %s where uid=?' % (cls.table, )
        _data = execute(sql, (uid, ))
        return len(_data) > 0

    def get(cls, uid, data={}):
        "get database record and return it as an object"
        if not data:
            # send uid as a proper SQLite parameter
            sql = f"select * from {cls.table} where uid=?"
            _data = execute(sql, (uid, ))
            if not _data:
                raise RecordNotFoundError(cls.table, uid)
            data = _data[0]
        ob = cls()

        #create the instance attributes
        ob.__dict__ = data

        # clear the change queue, so the defaults are not needlessly updated
        ob._v_changed = {}

        # call __init__
        if hasattr(cls.__bases__[0], '__init__'):
            cls.__bases__[0].__init__(ob)

        return ob

    __getitem__ = classmethod(get)  #### works for instance, but not class :s
    get = classmethod(get)
    # allow for get overrides
    __get__ = get

    @classmethod
    def tryget(cls, uid):
        "a bombproof get()"
        try:
            return get(cls, uid)
        except:
            return None

    @classmethod
    def new(cls):
        "create a new database record and return it as a an object"
        id = execute(f'insert into {cls.table}(UID) values(NULL)')
        return cls.get(id)

    def delete(self, uid=0):
        "remove self (or the database record with given uid: uid is for retro compat.)"
        sql = f'delete from {self.table} where uid=?'
        execute(sql, (uid or self.uid, ))

    def clone(self):
        "create a clone of self, flush it,  and return it"
        ob = self.new()
        for k in (i for i in self._v_fields if i != 'uid'):
            setattr(ob, k, getattr(self, k))
        ob.flush()
        return ob

    def all_change(self):
        "mark all fields as changed where their value is not None"
        changed = dict((i, True) for i in self._v_fields
                       if getattr(self, i) is not None)
        self._v_changed.update(changed)

    def pickle(self):
        "return pickled representation of the object's fields"
        return dumps(dict((k, getattr(self, k)) for k in self._v_fields))

    def pickle_update(self, pkl):
        "update object with pickled dict"
        d = loads(pkl)
        self.update(d)

    #list.permit='no way'  # disable direct web access
    #list=classmethod(list)

    @classmethod
    def list(cls,
             asObjects=True,
             sql='',
             sqlargs=(),
             like={},
             isin={},
             notin={},
             orderby='',
             where='',
             limit='',
             pager=-1,
             pagelength=20,
             what='*',
             _debug=False,
             **criteria):
        """return list of objects (if obs) or data filtered by these criteria
       sql with asObjects=True requires 'select *' to give fully valid objects, so 'what' should not be used with asObjects=False

    Parameters:
    cls = class of current object
    asObjects 
      = False: return a list of dictionaries 
      = True: return list of objects of the current class
    sql = full sql query. Parameters to be sent as tuple via sqlargs  
    sqlargs = tuple containing values to substitute into sql/where parameters.
    like = dict of form {fieldname:matchpattern}
      maps to sql 'fieldname LIKE "matchpattern" and otherfieldname LIKE "othermatchpattern"'
    isin = dict of form {fieldname:list-of-values}: filedname IN values
    notin = dict of form {fieldname:list-of-values}: fieldname NOT IN values 
    orderby = sql ORDER BY clause
    where = sql WHERE clause. Parameters to be sent as tuple via sqlargs
    limit = sql LIMIT clause
    pager = Start page of results divided by page length. Overrides limit parameter
    pageLength = length of pages - use in combination with pager parameter
    what = fields to be returned by query - deprecated by CJH 2013 as a security risk:
      (per CJH) we can't use sql args with field names so this is in practise a minute risk..
    _debug = if True print prepared sql statement
    **criteria = remaining field value pairs map to WHERE statement assersions which must all be true

      eg.  self.list(x=5,y='something') -> 'WHERE x=5 and y="something"'
    """
        # make sure we don't combine non-default 'what' with asObjects=True
        if asObjects and what != '*':
            raise ListParameterConflictError

    # sql overrides all other parameters except sqlargs
        if sql:
            # no further action required
            pass
        else:
            # build up sql from criteria
            if where:
                sqlparts = [where]
                sqlargs = list(sqlargs)
            else:
                sqlparts = []
                sqlargs = []

            # criteria
            if criteria:
                #       print">>>>>> criteria items >>>>>"
                #       for k,v in criteria.items():
                #         print k,v
                criteria_sql, criteria_args = cls.sql_quoted_pairs(
                    list(criteria.items()), link='and')
                sqlparts.append(criteria_sql)
                sqlargs += criteria_args

            # like
            if like:
                like_sql, like_args = cls.sql_quoted_pairs(
                    list(like.items()), op='like', link='and')
                like_sql = like_sql.replace("%", "%%").replace(
                    '%%s', '%s')  # double the % wildcard (but not any %s)
                sqlparts.append(like_sql)
                sqlargs += like_args

            # isin
#            print('>>>>>> isin: ',isin)
            for isin_field, isin_values in list(isin.items()):
                if not isin_values:
                    continue
                isin_sql = f' `%s` IN (%s)' % (
                    isin_field, ', '.join(['?'] * len(isin_values)))
                isin_args = list(isin_values)
                sqlparts.append(isin_sql)
                sqlargs += isin_args

            # notin
#            print('>>>>>> isin: ',isin)
            for notin_field, notin_values in list(notin.items()):
                if not notin_values:
                    continue
                notin_sql = f' `%s` NOT IN (%s)' % (
                    notin_field, ', '.join(['?'] * len(notin_values)))
                notin_args = list(notin_values)
                sqlparts.append(notin_sql)
                sqlargs += notin_args

            # orderby
            if orderby:
                orderby = " ORDER BY %s" % orderby

            # limit and paging
            if pager == -1:
                limit = limit and 'LIMIT %s' % limit or ''
            else:
                start = int(pager) * int(pagelength)
                limit = 'LIMIT %d,%d' % (start, int(pagelength))

            # We should have a complete query.  Convert args into a tuple.
            where = sqlparts and 'where' or ''
# evoke version was:
#            whereclauses = (' and '.join(sqlparts)).replace("%", "%%").replace(
#                '%%s', '%s')  # double the % wildcard (but not any %s)
            whereclauses = (' and '.join(sqlparts))
            sql = f'select {what} from {cls.table} {where} {whereclauses} {orderby} {limit}'
            sqlargs = tuple(sqlargs)
        # ready to go
        # optionally show our query.
        if _debug:
            print("LIST:", sql)
            print("LIST:", sqlargs)
        # execute query
        data = execute(sql, sqlargs)
        # optionally show the number of instances returned.
        if _debug:
            print("DATA COUNT:", len(data))
        # convert to objects if required
        if asObjects:
            data = [cls.get(i['uid'], data=i) for i in data]
        return data

    @classmethod
    def count(cls,
              like={},
              isin={},
              orderby='',
              where='',
              limit='',
              **criteria):
        """return count of data filtered by these criteria
    """
        return cls.list(
            asObjects=False,
            like=like,
            isin=isin,
            orderby=orderby,
            where=where,
            limit=limit,
            what="count(uid)",
            **criteria)[0]["count(uid)"]

    @classmethod
    def list_int(cls,
                 item='uid',
                 like={},
                 isin={},
                 orderby='',
                 where='',
                 limit='',
                 **criteria):
        """return list of integers from item (column) filtered by these criteria
    """
        return [
            int(i[item])
            for i in cls.list(
                asObjects=False,
                sql="",
                like=like,
                isin=isin,
                orderby=orderby,
                where=where,
                limit=limit,
                what="`%s`" % item,
                **criteria)
        ]

    @classmethod
    def max(cls,
            item='uid',
            like={},
            isin={},
            orderby='',
            where='',
            limit='',
            **criteria):
        """return max of item (column) filtered by these criteria
    """
        what = 'max(%s)' % item
        return cls.list(
            asObjects=False,
            sql="",
            like=like,
            isin=isin,
            orderby=orderby,
            where=where,
            limit=limit,
            what=what,
            **criteria)[0][what]

    @classmethod
    def min(cls,
            item='uid',
            like={},
            isin={},
            orderby='',
            where='',
            limit='',
            **criteria):
        "return min of item (column) filtered by these criteria"
        what = 'min(%s)' % item
        return cls.list(
            asObjects=False,
            sql="",
            like=like,
            isin=isin,
            orderby=orderby,
            where=where,
            limit=limit,
            what=what,
            **criteria)[0][what]

    @classmethod
    def sum(cls,
            item='uid',
            like={},
            isin={},
            orderby='',
            where='',
            limit='',
            **criteria):
        "return sum of item (column) filtered by these criteria"
        what = 'sum(%s)' % item
        return cls.list(
            asObjects=False,
            sql="",
            like=like,
            isin=isin,
            orderby=orderby,
            where=where,
            limit=limit,
            what=what,
            **criteria)[0][what]

    # export / import
    def for_export(self, extras=[]):
        "convert object to a dictionary (for pickling) - include only standard fields, plus given extras"
        return dict((k, v) for k, v in list(self.__dict__.items())
                    if k in self._v_fields or k in extras)

def makeDataClass(Schema):
    "Set up a custom SQL data class"
    data = {
       'table':f"`{Schema.table}`"
      , '_v_schema': Schema._v_schema
      , '_v_fields': list(Schema._v_schema.keys())
#      , '_v_textkeys': Schema._v_textkeys
      }
    return type(Schema.table.capitalize(), (MassProducedSQLDataObject, ),data)


# TESTS
if __name__ == '__main__':
    # Data Object
    c = DataObject(['camelid', 'ox'])
    # let's start with a clear run
    assert c.flush() == {}, "Flush should be empty"
    # Check for a valid change
    c.camelid = 'llama'
    assert c.flush() == {'camelid': 'llama'}, "setattr noworks"
    assert c.flush() == {}, "Flush should be empty"
    # _v_ attributes don't register change
    c._v_gonewiththewind = 'Frankly'
    assert c.flush() == {}, "Flush should be empty"
    c.gnu = 'GNU'
    assert c.flush() == {}, "Flush should be empty"
    c.update({'ox': 'OXEN'})
    assert c.flush() == {'ox': 'OXEN'}, "update noworks"
    c.update({'ox': 'OXEN'})
    del c.ox
    assert c.flush() == {}, "Flush should be empty"

    # filter flush by only
    c.camelid = 'Alpaca'
    res = c.flush(only=['bison'])
    assert res == {}, "Flush should be empty"
