""" grace html generation:

new age templating for grace

consists of 2 code files:
- evo.py : template parser
- html.py : decorator for template-calling-functions (ie python interface)

a 2-pass process:
1: evo=>pyc  (done only once per template, unless the template is altered)
  - parse the source template into python
  - compile the python into byte code
  - cache it
2: pyc=>html (each call ie on the fly)
  - eval the python in the current context

allows for multiple apps having different versions of same-name templates

issues:
- let only creates variables in the local context (albeit using globals()!?!) - any include does not see them.....
- let only allows simple variables: would be nice to allow "instance.var=whatever"
- let and include blocks have the indentation of the definition context, not the use context - more hassle to fix than it is worth?
- cannot use += for local variables, eg count+=1 (would be nice..)
- better error pinpointing would be nice
"""
debug = False  #slows things down considerably, and is more verbose...
#debug=True
sparse = True  #keep the output lean? ie no indents or comments

from os import stat
from os.path import lexists
import sys
from gettext import gettext

from grace import lib

# EVO error handling


class EvoError(lib.Error):
    "EVO ERROR"


class EvoTemplateNotFound(EvoError):
    'template "%s" not found'


class EvoParseError(EvoError):
    '%s on line %s in %s : %s'


class EvoSyntaxError(EvoError):
    'line %s in %s : %s ^ %s'


# Evo() parser,compiler, and renderer


class EvoCacheObject(object):
    "stores python code and timestamp"

    def __init__(self):
        self.pyc = ""


class Evo(object):
    "a GRACE template"

    pycache = {}
    pathcache = {}

    def __init__(self, filename):
        "initialise Evo instance - allow for absence of gettext."
        self.filename = filename

    def __call__(self, ob, req, wrapper='', gettext=lambda x: x):
        """ generate HTML
    - allow for multiple apps having different template versions within each Evo instance - we ony get the app data at call time
    - wrapper can be passed as req.wrapper or as wrapper: wrapper=None means no wrapper
    """
        self.gettext = gettext
        # get the template path for this app
        # is it on the cache?
        self.key = ob.Config.appname + '.' + self.filename
        self.path = self.pathcache.get(self.key, "")
#        print(1,self.path)
        if not self.path:  # first time only.. get the paths
            # note: we couldn't do this earlier, as we lacked the grace and app filepaths
            # firstly: use the local class template, if there is one
            klass = self.filename.split("_", 1)[0]
            self.path = '%s%s/evo/%s' % (ob.Config.app_filepath, klass,
                                         self.filename)
#            print(2,self.path)
            # otherwise, is there a local template?
            if not lexists(self.path):
                self.path = ob.Config.app_filepath + 'evo/' + self.filename
#                print(3,self.path)
                # otherwise, is there a class template in grace (base)?
                if not lexists(self.path):
                    self.path = '%s%s/evo/%s' % (ob.Config.grace_filepath,
                                                 klass, self.filename)
#                    print(4,self.path)
                    # otherwise, use the grace template
                    if not lexists(self.path):
                        self.path = ob.Config.grace_filepath + 'evo/' + self.filename
#                        print(5,self.path)
                        # if that doesn't exist, raise an error
                        if not lexists(self.path):
                            print(f'ERROR: template {self.path} not found')
                            raise EvoTemplateNotFound(self.filename)
            self.pathcache[self.key] = self.path
        #get the CacheObject for this path, containing the python code and timestamp
        cob = self.pycache.get(self.path, EvoCacheObject())
        #and parse the template to a python expression
        # ... unless we already have python code and the timestamp matches
        if (not cob.pyc) or (cob.timestamp != stat(self.path).st_mtime):
            # set the timestamp
            cob.timestamp = stat(self.path).st_mtime
            # parse the template into python code
            cob.pyc = self.parse(self.path, ob, req)
            # compile the python code to the cache
            #      print("compiling %s : " % self.filename,cob.pyc)
            if not debug:
                try:
                    cob.pyc = compile(cob.pyc, '<string>', 'eval')
                except SyntaxError as inst:
                    p = inst.offset
                    t = inst.text
                    raise EvoSyntaxError(
                        "char %s" % p, "evo pycode for %s" % self.filename,
                        t[max(0, p - 40):p], t[p:min(p + 40, len(t) - 1)])
        # sort out the wrapper
        if wrapper is not None:
            wrapper = wrapper or req.get('wrapper', 'wrapper.evo')
        # and run the python code from the cache
        res = self.wrap(cob.pyc, ob, req,
                        wrapper) if wrapper else self.evaluate(
                            cob.pyc, ob, req)
        return res

    def evaluate(self, pyc, ob, req):
        "run the python code, returning the result"
        ns = dict(namespace)

        #mygettext = getattr(self, 'gettext', gettext)
        mygettext = req.gettext

        ns.update({'req': req, 'self': ob, 'lib': lib, '_': mygettext})
        try:
            return eval(pyc, ns, {})
        except SyntaxError as inst:
            p = inst.offset
            t = inst.text
            raise EvoSyntaxError("char %s" % p, 'evo bytecode',
                                 t[max(0, p - 40):p],
                                 t[p:min(p + 40, len(t) - 1)])
        except Exception as inst:
            raise

    def wrap(self, pyc, ob, req, wrapper):
        ""
        ob._v_content_pyc = pyc
        return Evo(wrapper)(ob, req, wrapper=None) or ' '

    def parse(self, codefile, ob, req):
        "evo => python parser "
        
#        print("PARSING:", codefile)
        # create a simple stack
        stack = []

        def push(code, indent, kind='tag',stack=stack):
            stack.append((code, indent, kind))
#            print(' code:', code, '  indent:', indent, '  kind:', kind, '  stack: ', stack)

        def pop(stack=stack):
            top = stack[-1]
            del stack[-1]
            return top

        # fetch the code from the file, and de-tab it
        code = open(codefile, 'r').read().expandtabs(8)
        # use a list to gather the output
        output = []
        # init the other vars we will need..
        indent = 0
        line_number = 0
        continuation = ''
        # start parsing
        for codeline in [l.rstrip() for l in code.split("\n")] + ["end.of.file"]:
            line_number += 1
            # handle continuation (pass code to next line via `continue`)
            codeline = continuation + codeline
            if codeline.endswith('\\'): 
                continuation = codeline[:-1]
                continue
            continuation = ''
            rawline = codeline.lstrip()
            if rawline and not rawline.startswith('#'):  #ignore comments
                xindent = indent
                if rawline == "end.of.file":
                    indent = stack[0][1]  #dedent to first indent
                    rawline = ''
                else:
                    indent = len(codeline) - len(rawline)
                if stack and indent <= xindent:
                    while True:
                        xxindent = xindent
                        end, xindent, kind = pop()
                        if (end is not None):
                            if kind == 'logic':
                                output.append(end)
                            else:
                                comma = "," if (end and (output[-1][-1] != "(")) else ""
                                output.append(f"{comma}{end}).out({len(stack)})")
                        if stack and stack[0][1] != 0:
                            raise EvoParseError("STACK WRONG", line_number, self.path,
                                                rawline)
#              print(f"STACK WRONG line {line_number} in {self.path,rawline} : {stack}"
                        if indent >= xindent or not stack:
                            if indent > xindent:
                                raise EvoParseError("INVALID DEDENT", line_number,
                                                    self.path, rawline)
#              print(f"INVALID DEDENT line {line_number} in {self.path} : {rawline}", xindent,indent)
                            elif not stack and indent < xindent:
                                raise EvoParseError("STACK EMPTY", line_number,
                                                    self.path, rawline)
#              print(f"STACK EMPTY line {line_number} in {self.path} : {rawline}", xindent,indent)
                            break
                if rawline:
                    kind = 'logic'
                    if rawline.startswith("'") or rawline.startswith('"'):  #raw text
                        start = rawline
                        end = None
                        kind = 'raw'
                    elif rawline.startswith('('):  #expression
                        start = rawline
                        end = None
                        kind = 'expr'
                    elif rawline.find(":") > -1:
                        start, end = rawline.split(":", 1)
                        if start[:4] == 'for ':
                            # set end and start
                            end = f') {start}))'
                            start = '("\\n".join(('
                        elif start[:3] == 'if ':
                            # set end and start
                            end = ') or " ")'  #this makes sure that the whole if-elif-else clause is True
                            start = f'(({start[3:]}) and ('
                        elif start[:4] == 'elif':
                            end = output[-1] + ')'
                            #drop the end from the previous if or elif
                            del output[-1]
                            #reset xindent
                            xindent = xxindent
                            #set start
                            start = f' or " ") or (({start[5:]}) and ('
                        elif start[:4] == 'else':
                            end = output[-1]
                            #drop the end from the previous if or elif
                            del output[-1]
                            #reset xindent
                            xindent = xxindent
                            #set start
                            start = ' or " ") or ('
                        else:  #standard tag
                            start += '('
                            kind = 'tag'
                    elif rawline.find("=") > -1:  #we have a let
                        # get let arguements
                        start, end = rawline.split("=", 1)
                        # remove any whitespace
                        start = start.strip()
                        end = end.strip()
                        #error check
                        f = start.find(' ')
                        if f > -1:
                            raise EvoSyntaxError(line, self.path, x[:f], x[f:])
                        #expand start and end, ready for output
                        start = f'let(globals(),{start}='
                        end = f'{end})'
                    elif rawline.endswith('.evo'):
                        parts = rawline.rsplit('.', 2)
                        start = f'include("{parts[-2]}.evo",{parts[0] if len(parts) == 3 else "self"},req)'
                        end = None
                        kind = 'include'
#                    elif rawline == 'content.here':  #wrapper include
                    elif rawline in ('content.here', '__content__()'):  #wrapper include, allowing for nevo approach also
                        start = 'content(self,req)'
                        end = None
                        kind = 'include'
                    else:
                        start = rawline
                        end = None
                        kind = 'expr'

                    # check syntax of expressions
                    if kind == 'expr': 
                        try:  #this will break if there are any globals - we are just looking for syntax errors
                            eval(rawline)
                        except SyntaxError as inst:
                            raise EvoSyntaxError(line_number, self.path,
                                                 rawline[:inst.offset],
                                                 rawline[inst.offset:])
                        except:  #ok
                            pass

                    # push the end, and output the start
                    push(end, indent, kind)
                    prefix="+'\\n'+" if (output and (xindent == indent)) else ""
                    output.append(f'{prefix}{start}')

        # annotate the output with codefile start/stop messages
        if output and not sparse:
            if output[0].find("<!DOCTYPE") >= 0:
                output.insert(1, f"+'\\n<!-- start of {codefile} -->\\n'")
            else:
                output.insert(0, f"'\\n<!-- start of {codefile} -->\\n'+")
            output.append(f"+'\\n<!-- end of {codefile} -->\\n'")

        # glue together and return
        py = "".join(output)
        if debug:
            print("PYTHON:", py)
        return py


#################################################################
# evo template language definition: support classes and functions
#################################################################


def tag(name, _base='', _clean=False, **defaults):
    "class factory to make tag classes"
    singleton = name[-1] == "/" and "/"
    if singleton:
        name = name[:-1]
    self = type(name, (_base or basetag, ), defaults)
    self.defaults = dict(defaults)
    if _clean:
        self.template = "<%s%s%s>" % (name, "%(attributes)s",
                                      singleton or (">%s</%s" %
                                                    ("%(content)s", name)))
    elif sparse:
        self.template = "<%s%s%s>" % (name, "%(attributes)s",
                                      singleton or (">\n%s\n</%s" %
                                                    ("%(content)s", name)))
    else:
        self.template = "%s<%s%s%s>%s" % ("%(indent)s", name, "%(attributes)s",
                                          singleton or (">\n%s\n%s</%s" %
                                                        ("%(content)s",
                                                         "%(indent)s", name)),
                                          "%(_comment)s")
    return self


class basetag(object):
    "foundation class for tag classes"

    def __init__(self, _content="", **attributes):
        self.content = _content
        self.attributes = dict(self.defaults)  #separate copy
        self.attributes.update(
            attributes)  #now, attributes contains everything we need
#    print "ATTRIBUTES:",self.attributes

    keymap = {
        'cls': 'class',
        'equiv': 'http-equiv',
        'for_id': 'for',
    }

    def out(self, indent=0):
        def subst(k):
            "substitute attribute name"
            if k in self.keymap:
                return self.keymap[k]
            # convert _ to -
            return k.replace('_', '-')

        attributes = "".join([
            (' %s="%s"' % (subst(k), str(v).replace('"', "&quot;")))
            for (k, v) in self.attributes.items()
            if v or (k not in ('checked', 'selected', 'disabled', 'itemscope'))
        ])
        return self.template % dict(
            indent=" " * indent,
            attributes=attributes,
            content=self.content,
            _comment=not sparse and 'id' in self.attributes and
            (" <!-- end of %s -->" % self.attributes.get('id')) or "")


#HTML tags
a = tag('a')
article = tag('article')
b = tag('b')
big = tag('big')
body = tag("body")
br = tag("br/")
button = tag('button')# formerly had default: type="submit"
#caption=tag('caption')
center = tag('center')  # deprecated - use CSS
cite = tag('cite')
dd = tag('dd')
dl = tag('dl')
dt = tag('dt')
div = tag("div")
fieldset = tag('fieldset')
form = tag('form') # formerly had default: method='post'
h1 = tag('h1')
h2 = tag('h2')
h3 = tag('h3')
h4 = tag('h4')
h5 = tag('h5')
h6 = tag('h6')
head = tag("head")
hr = tag('hr/')
html = tag("html")
i = tag("i")
#img=tag('img/',border='0')
img = tag('img/')
input = tag('input/') # # formerly had defaults: type='text', value=''
label = tag('label')
legend = tag('legend')
li = tag('li')
link = tag('link/')
meta = tag('meta/')
noscript = tag('noscript')
ol = tag('ol')
option = tag('option')
p = tag("p")
pre = tag('pre')
script = tag('script') # formerly had explicit default: type="text/javascript" - now part of HTML standards
select = tag('select')
small = tag("small")
span = tag("span")
table = tag('table')
tbody = tag('tbody')
td = tag('td')
textarea = tag('textarea', _clean=True) # _clean is for internal evo.py use...
tfoot = tag('tfoot')
th = tag('th')
thead = tag('thead')
title = tag("title")
tr = tag('tr')
ul = tag('ul')
nav = tag("nav")

# obsolete but useful
style = tag('style')
font = tag('font')
strong = tag('strong')

section = tag("section")
header = tag('header')
footer = tag('footer')
article = tag('article')

# Bootstrap
css = tag('link', rel="stylesheet", type="text/css")
row = tag('div', cls='row')
container = tag('div', cls="container")
fluid = tag('div', cls="container-fluid")
col1 = tag('div', cls="col-md-1")
col2 = tag('div', cls="col-md-2")
col3 = tag('div', cls="col-md-3")
col4 = tag('div', cls="col-md-4")
col5 = tag('div', cls="col-md-5")
col6 = tag('div', cls="col-md-6")
col7 = tag('div', cls="col-md-7")
col8 = tag('div', cls="col-md-8")
col9 = tag('div', cls="col-md-9")
col10 = tag('div', cls="col-md-10")
col11 = tag('div', cls="col-md-11")
col12 = tag('div', cls="col-md-12")
formgroup = tag('div', cls="form-group")


## logic

def let(namespace, **args):
    "allows local variable assignment, and macro blocks"
    namespace.update(args)
    return ""


def include(filename, ob, req):
    "call another template"
    return Evo(filename)(ob, req, wrapper=None)


def content(ob, req):
    "wrapper include"
    #  return Evo("").evaluate(ob._v_content_pyc,ob,req)
    #force to str to get rid of "WARNING: UNICODE STRING in evo/wrapper.evo - converted to str" errors in log
    return str(Evo("").evaluate(ob._v_content_pyc, ob, req))


namespace = locals()
