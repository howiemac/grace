"""simple HTTP server for Grace

Ian Howie Mackenzie March 2020
"""

import sys, os
from http import server, cookies
from os import getpid
from urllib.parse import parse_qs
from cgi import parse_header, parse_multipart

from grace.serve.dispatch import Dispatcher
from grace.serve.req import Req


class handler(server.SimpleHTTPRequestHandler):
    """Grace custom HTTP request handler

    """
    basepath="."

#    def parse_request(self):
#        """ override the Base handler to identify non-flat-file commands
#
#        The request should be stored in self.raw_requestline; the results
#        are in self.command, self.path, self.request_version and
#        self.headers.
#        """
#        return server.BaseHTTPRequestHandler.parse_request(self)

    def do_method(self):
        """
        handle the request: same for GET or POST (for now..)
        - start building our req dictionary for args
          - the req format is {key:value, ..,  _v_request:request}
        - put the remaining request data in req._v_request dictionary
        - call the dispatcher
        - send the response to the browser
        """
#        print(f"{self.command} - {self.path} - {self.request_version}")
#        print(f"{dict(self.headers.items())}")
        # build _v_request
        request=Req()
        request.update(self.headers.items())
        request.path = self.path
        request.uri = self._v_uri_args[0]
        request.method = self.command
        request.clientproto = self.request_version
        # allow other modules to set responses
        request.setResponseCode = self.send_response
        request.setHeader = self.send_header
        request.endHeaders = self.end_headers
        # cookies
        request.setCookie = self.set_cookie
        if self.headers["Cookie"]:
          request.cookies = dict([c.split('=') for c in self.headers['Cookie'].split('; ')])
        else:
          request.cookies= {}
        # build req
        req=Req()
        req['_v_request'] = request
#        print('####early req: ',req)
        req.update(self.get_args()) # GET and POST args
        # fetch the cache (so it can be accessed via req)
        req._v_cache = dispatcher.cache
#        print('>>>>>>>>>> REQ')
#        for (k,v) in req.items():
#          print(k,':',v)
#          print('  ')
#        print('REQ:  ',req)
#        print('CACHE BEFORE: ',dispatcher.cache)
        # call the dispatcher
        response = dispatcher.response(req)
        # store any cache changes
        dispatcher.cache = req._v_cache
#        print('CACHE AFTER: ',dispatcher.cache)
        # handle the response
        if response:
            self.wfile.write(bytes(response,encoding='utf-8'))

    def do_GET(self):
        """Serve a GET request."""
        # Split path into uri and args (if any)
        self._v_uri_args=self.path.split('?')
#        print(self._v_uri_args)
        # Handle the flat files first (if Apache hasn't already fielded these)
        # Expand self.path and call SimpleHTTPRequestHandler.do_GET()
        uri=self._v_uri_args[0]
        config = dispatcher.app.classes['Config']
        path=''
        if uri.startswith("/site/"):
            path = f"{config.site_filepath}{uri[6:]}"
        elif uri == "/favicon.ico" or uri.endswith('.html'):
            path = f"{config.site_filepath}{uri[1:]}"
        try: # catch broken pipe error
#        if True:
            if path:
                self.path=path
 #               print("config.site_filepath: ", config.site_filepath)
 #               print("URI: ",uri," path: ",self.path)
                return server.SimpleHTTPRequestHandler.do_GET(self)
            else:
                return self.do_method()
        except BrokenPipeError as e:
            print('ERROR: ',e)
        except:
            print("unexpected grace server error:", sys.exc_info()[0])
#            raise

    def do_POST(self):
        """Serve a POST request: same as GET (for now..)""" 
        return self.do_GET()

    def get_args(self):
        """ returns dictionary of args, combining query and POST data
          (based on code from twisted/web/http.py)
        """
        # get GET query string..
        s = self._v_uri_args
        get = len(s)>1 and s[1].strip() or ''
        # get POST query string or multipart/form data
        post = ""
        multi = None
        ctype = self.headers.get('Content-Type')
#        if ctype is not None:
#            ctype = ctype[0]
        if ctype and (self.command == 'POST'):
            key, pdict = parse_header(ctype)
            if 'boundary' in pdict:
                pdict['boundary']=bytes(pdict['boundary'],'ascii')
#            print('>>> key:','  pdict:',pdict)
            try:
                clen = int(self.headers.get('Content-Length'))
            except:
                raise # TESTING ONLY - REMOVE THIS
                clen = 0
            if key == "application/x-www-form-urlencoded":
                post = clen and str(self.rfile.read(clen),'utf-8').strip() or ""
            elif key == "multipart/form-data":
                try:
                    pdict['CONTENT-LENGTH'] =  clen # fix (see the mysterious CGI.parse_multipart code)
                    multi = parse_multipart(self.rfile, pdict)
                except:
                    raise # TESTING ONLY - REMOVE THIS
                    pass
        if post and get: post += '&'
        rd = parse_qs(post + get,keep_blank_values=True)
        if multi:
            rd.update(multi)
#        print('>>>>>>', rd)
        #remove multiple occurrences - ie take first value only from each value list
        args = {}
        for (k,v) in list(rd.items()):
            args[k] = v[0]
#        print(">>>>>>>args: ",args)
        return args

    def set_cookie(self,id,data="",
                   expires=None,
                   domain=None,
                   path='/',
                   max_age=None,
                   comment=None,
                   secure=False,
                   httponly=False,
                   version=None,
                   samesite=None):
        """set a cookie header """
        c = cookies.SimpleCookie()
        c[id] = data
        if expires is not None:
            c[id]["expires"] = expires # date/time string
        if domain is not None:
            c[id]["domain"] = domain
        if path is not None:
            c[id]["path="] = path
        if max_age is not None:
            c[id]["max_age"] = max_age # seconds
        if comment is not None:
            c[id]["comment"] = comment
        if secure:
            c[id]["secure"] = True
        if httponly:
            c[id]["httponly"] = True
        if version:
            c[id]["version"] = version
        if samesite is not None:
            c[id]["samesite"] = samesite # "Lax" or "Strict"
        self.send_header(c.output())

def start():
    """ runs the HTTP server
    """
    config = dispatcher.app.classes['Config']
    server_address = ('', int(config.port)) # O/S DOMAINS PER DISPATCHER SHOULD COME INTO THIS?? RATHER THAN '127.0.0.1'
    httpd = server.HTTPServer(server_address,handler) 
    # note: cannot use server.ThreadingHTTPServer as this breaks SQLite: see https://sqlite.org/faq.html#q6
    # OR see https://sqlite.org/faq.html#q5 for info on multiprocessing with SQLite
 #    httpd.socket.getsockname()

    open('server.pid','w').write(repr(getpid()))

    # license
    print(config.copyright)
    # message
    print(f'serving HTTP on port {config.port}')
    # serve
    httpd.serve_forever()

# startup
sys.path.insert(0, os.path.abspath('.')) # fix the path
dispatcher = Dispatcher() # create one single dispatcher instance
dispatcher.cache = {} # create a cache
