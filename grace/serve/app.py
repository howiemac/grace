"""
The App class constructs the custom GRACE/EVOKE "klass" structure for the app:
    - create a Config class for an app, from config.py modules
    - set up application structure based on the Config object definitions (ie Schema subclasses)
    - call the database creation/maintenance code
    - mix together DataObject-esque classes

NOTE - grace/config.by acts as a default config for all apps, and so is loaded for each app
     - the local config.py can override these defaults

A single instance of the App class is created by dispatch.py at startup.

Ian Howie Mackenzie (April 2020)
"""

from inspect import isclass
from os.path import lexists, split
from os import getcwd
from sys import modules,path

from grace import grace_filepath
from grace.data import connect, makeDataClass, patch, schema
from .url import Url
from .baseobject import Baseobject

class App:

    def __init__(self):
        "get the configuration for this app"
        #print('intialising App for ',self)

        # first sort out the app name and filepath: ASSUMES WE ARE IN THE DIRECTORY CONTAINING THE APP
        app_filepath = getcwd()+'/'
        appname = split(app_filepath[:-1])[1]
#        print(">>>>", "app_filepath =" ,app_filepath)
#        print(">>>>", "app name =" ,appname)

        self.app_path = "" # this is the python module path: (empty string  because WE ARE IN THE DIRECTORY CONTAINING THE APP)

        # build the schemaClasses and config dictionaries - <app>/config.py overrides grace/config.py
        self.schemaClasses = {}
        # start the config dictionary, with grace_filepath from grace.__init__.py
        self.config = dict(grace_filepath=grace_filepath)
#        print (f">>>>>>>>>>>>>>>> grace_filepath: {grace_filepath}")
        # get the default config
        if appname=="grace":
            self.get_config('config', 'grace.')
        else:
            # get the grace config
            self.get_config('config', 'grace.')
            # get the app overrides
            self.get_config('config', '')

        # convert self.config to a class
        self.Config = type("Config", (object, ), self.config)

        # add app name
        self.Config.appname = appname
        # and path
        self.Config.app_filepath = app_filepath
        # add grace version
        self.Config.grace_version=open(f"{grace_filepath}VERSION").read().split('=')[1].strip()

        # make sure we have a database
        db = self.Config.database or appname
        # append ."db" to the database name, if not already there
        if not db.endswith('.db'): db+='.db'
        # add database to Config
        self.Config.database = db
        # connect to the database - if it doesn't exist, create it
        # O/S a suitable path should be prepended to the database name, and permissions dealt with...
        connect(self.Config.database)

        ## do any pre-schema patching (DISABLED)
        # patch.pre_schema(self)

        # create the objects and database tables
        self.classes = {"Config": self.Config}
        self.make_objects(list(self.schemaClasses.items()))
        #interlink classes and make them globally available
#        print self.classes.items()
        classes = list(self.classes.items())
        for targetid, targetcls in classes:
            if targetcls.__name__ != 'Config':
                for sourceid, sourcecls in classes:
                    setattr(targetcls, sourceid, sourcecls)
            # make classes available in globals (mostly for interactive testing)
            globals()[targetid] = targetcls

        # add other convenient config items
        self.Config.domain = self.Config.domain or self.Config.domains[0]
        # ASSUME the system is running on port 80, as far as the outside world is concerned:
        self.Config.urlhost = self.Config.urlhost or 'http://' + self.Config.domain
        # add version and copyright message
        self.Config.copyright = f"""
GRACE version {self.Config.grace_version}
Copyright © 2023 Ian Howie Mackenzie
All rights reserved.
"""
        ## do any post-schema patching (DISABLED)
        #patch.post_schema(self)

    def get_config(self, module, path):
        "extract the schema classes, and the config items from the config modules"
        #print(f"GETTING CONFIG:{path+module}")
        try:
            config = __import__(path + module, globals(), locals(),
                                '__main__')  #import the module
            for k, v in list(config.__dict__.items()):
                if k not in schema.__dict__:
                    if isclass(v) and issubclass(v, schema.Schema):
                        self.schemaClasses[k] = v
                        #print('klass ',k,' = ',self.schemaClasses[k])
                    else:
                        self.config[k] = v
                        #print('config item ',k,' = ',self.config[k])
        except Exception as e:
            #print(f"ERROR IN {path_module}.py : " % (path+module,),e)
            raise

    def make_objects(self, schemaClasses):
        ""
        #print("MAKING OBJECTS: ",schemaClasses)
        #print("app_filepath:", self.Config.app_filepath)
        tables = {}
        for (k, c) in schemaClasses:
            #      print "....object...",k,c.__name__
            c.table = getattr(c, 'table', c.__name__.lower())
            tables[c.table] = (k, c)  # duplicate table - ie a subclass -
            # with same table name, will override the base class,
            # so that only the subclass is added to the database
        # add each table to the database
        for (k, c) in list(tables.values()):
            c.build_database(self.Config.database)
            klass = c.__name__.capitalize(
            )  #force capitalisation for class / object names - consistent, and avoids naming conflicts with methods, keywords etc
            # look for the py file locally first, if not there then it must be in grace
            modulefile=self.Config.app_filepath + klass
            if (lexists(modulefile) or lexists(modulefile + '.py')): #allow for py file or folder
                module = self.app_path + klass
            else:
                module = 'grace.' + klass
            self.make_object(k, module, klass, c)

    def make_object(self, id, module, klass, schemaClass):
        "builds the self.classes dictionary"
        # set up class bases
        bases = []
#        print ("MAKING OBJECT for module:",module)
        bases.append(
            getattr(__import__(module, globals(), locals(), klass),
                    klass))  #yuk... but it works...for grace.module.klass too
        if schemaClass._v_columns or schemaClass.table:  # DataObject
            bases.append(makeDataClass(schemaClass))
        bases.extend(
            [Url, Baseobject]
        )  #do these last so we can override their attributes and methods in the module class
        #make the object
#        print (id)
#        for base in bases: print ('X', base)
        cls = self.classes[id] = type(str(id), tuple(bases), {})
        # trigger __class_init__ if it exists
        if hasattr(cls, '__class_init__'):
            cls.__class_init__()




