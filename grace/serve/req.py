"""
grace request object

by Ian Howie Mackenzie and Christopher John Hurst (2007 to 2020)
"""
import sys
import time
from copy import copy
from urllib.parse import quote

from grace.lib import DATE, http_date

class Reqbase(dict):
    """ extend the dict object with our own extra methods
"""

    def redirect(self, url, permanent=False, anchor=""):
        """Cause a redirection without raising an error
        - accepts full URL (eg http://whatever...) or URI (eg /versere/Account/2/view)
        - retains anchor and messages
        """
        #safety first - defend against unicode, which will break both http and string matching
        if type(url) == bytes:
            url = str(url, 'utf8')
        url = str(url).strip()
#        print("REDIRECT:",url)
        if not url.startswith('http'):
            url = f"http://{self.get_host()}{url}"
#        print("REDIRECT full url:",url or "no url", " == anchor: ", anchor or "no anchor")
        if '#' in url:
            url, anchor = url.rsplit('#', 1)
        ats = self.error and ['error=%s' % quote(self.error)] or []
        if self.warning:
            ats.append('warning=%s' % quote(self.warning))
        if self.message:
            ats.append('message=%s' % quote(self.message))
        q = url.find('?') > -1 and "&" or "?"
        ats = ats and (q + '&'.join(ats)) or ""
        url = '%s%s%s%s' % (url, ats, anchor and '#' or '', anchor)
        # do the redirect
        self._v_request.setResponseCode(permanent and 301 or 302, None)
        self._v_request.setHeader('Location', url)
        self._v_request.endHeaders()
        return ""

    def set_cookie(self, id, data="", expires=None, **kwargs):
        """create or reset a cookie
           - for kwargs see handler().set_cookie in grace.serve.server
        """
        # translate expires from seconds to http date
        when = http_date(expires) if expires else None
        self._v_request.addCookie(id, data, when, **kwargs)

    def clear_cookie(self, id):
        "cookie is cleared by setting it to expire a year ago!"
        self.set_cookie(id, expires=-3600 * 24 * 365, max_age=0)

    def get_cookies(self):
        "dictionary of cookies"
        return self._v_request['cookies'] # O/S this has to be turned into a dictionary (in serve.py)

    def get_cookie_data(self,id):
        "returns the value (if any) for the given cookie id"
        return self.get_cookies().get(id,None)

    def get_host(self):
        "domain and port"
        return self._v_request['Host']

    def get_domain(self):
        "domain"
        return self.get_host().split(":")[0] #strip off the port

    def get_uri(self):
        "uri"
        return self._v_request["uri"]

    def get_user_agent(self):
        "user agent"
        return self._v_request['User-Agent']

    def __copy__(self):
        "make this copyable by copy.py"
        return self.__class__(copy(dict(self)))

class Req(Reqbase):
    """dict / object hybrid - CJH- stores form fields as a dictionary,
  but allows them to be set and got as req properties.
  WEAKNESS: if a form field uses a string method name as a key,
  then that field can only be set and got using req.[key] syntax
  ADVANTAGE:  allows backward compatibility, and the direct use of
  dictionary methods and operands
  """

    def __getattribute__(self, k):
        if hasattr(Reqbase, k):
            return dict.__getattribute__(self, k)
        else:  # do an implicit get
            return self.get(k, "")

    def __setattr__(self, k, v):
        if hasattr(Reqbase, k):
            dict.__setattr__(self, k, v)
        else:
            self[k] = v

def test():
    ""
    r = Req()
    # non-dict key
    r.pie = 400
    assert r.pie == r['pie'] == 400, "non dict keys noworks"
    # dict key
    r.copy = 401
    assert 'copy' not in r, "dict keys should not be items"
    assert r.copy == 401, "dict keys should be set as attributes"
    assert r.wrong == "", "invalid attribute should return an empty string "

if __name__ == '__main__':
    test()
