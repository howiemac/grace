""" request dispatcher
"""
from mimetypes import types_map
import traceback
import time
import sys
import _thread

from .app import App
from .url import Url
from .parse import Parser
from grace.lib import DATE, http_date

class dispatcherCode(Parser):
    ""

    def response(self, req):
        ""
        # confirm domain is valid
        domain = req.get_domain()
        if domain not in self.app.Config.domains:
            req.error = f"invalid domain {domain}"
            return self.doUnknown(req)

        # we will need config below...
        config = self.app.classes['Config']

        # create a handy reference to the homePage instance
        req.HOME = self.app.classes['Page'].get(int(config.default_page))

        # remove the prefix (urlpath), if any
        uri = req.get_uri()
        if config.urlpath and uri.startswith(config.urlpath):
            URI = uri[len(config.urlpath):]
        else:
            URI = uri

        # parse the request
        # run the URI through the parser
        uri_type, cls, uid, method = self.parseUri(URI)
        # save the method, in case we need it later (eg for return_to)
        req._v_method = method
        # get the default class, if any
        cls = cls or config.default_class

        # and dispatch appropriately...
        if uri_type == 'obj':
            return self.doObject(req, cls, uid or 1, method or 'view', uri)
        elif uri_type == 'namedobj':
            return self.doNamedObject(req, cls, uid or 1, method or 'view',
                                      uri)
        elif uri_type == 'cls':
            return self.doClass(req, cls, method, uri)
        elif uri_type == 'export':
            return self.doExport(req, cls, uid, method, uri)
        elif uri_type == 'default':
            return self.doDefault(req)
        else:
            req.error = "invalid URI type %s" % uri_type
            return self.doUnknown(req)


    def doObject(self, req, cls, uid, method, url):
        ""
        try:
            # be lax about class case sensitivity in urls...
            ob = self.app.classes[cls.capitalize()].get(int(uid))
            return self.doMethod(req, ob, method.replace(".", "_"), url)
        except Exception as e:
            msg = "ERROR with %s %s: %s" % (cls, uid, e)
            print(msg)
#            raise
            req.error = msg
            return self.doUnknown(req)

    def doNamedObject(self, req, cls, uid, method, url):
        "Handle object referenced by unique name not uid"
        try:
            klass = self.app.classes[cls.capitalize()]
            # make sure this klass has a uname field
            if 'uname' not in klass._v_fields:
                req.error = "%s does not support named objects" % cls
                return self.doUnknown(req)
            ob = klass.list(
                uname=uid)[0]  # this assumes that uname is always unique
            return self.doMethod(req, ob, method.replace(".", "_"), url)
        except:
            req.error = "unknown object %s/%s" % (cls, uid)
            return self.doUnknown(req)

    def doClass(self, req, cls, method, url):
        ""
        try:
            # be lax about class case sensitivity in urls...
            ob = self.app.classes[cls.capitalize()]
        except:
            req.error = "unknown class %s" % cls
            return self.doUnknown(req)
        return self.doMethod(req, ob, method.replace(".", "_"), url)

    def doExport(self, req, cls, uid, method, url):
        ""
        # be lax about class case sensitivity in urls...
        ob = self.app.classes[cls.capitalize()].get(int(uid))
        return self.doMethod(req, ob, 'export_eve', url)

    def doMethod(self, req, ob, method, url=''):
        "call the requested method, if permitted"
        # allow for .csv and other methods which have a dot extension
        # so the browser knows what to do
        method = method.replace(".", "_")
        # check that the function exists
        fn = getattr(ob, method, None)
        if fn is None:
            req.error = "unknown method %s" % method
            return self.doUnknown(req)
        # return the result of the function
        try:
            response = fn(req)
            if response: # eg, not a redirect 
                req._v_request.setResponseCode(200)
                req._v_request.setHeader('content-type', 'text/html')
                # prevent browser from using cache - expired a year ago!
                req._v_request.setHeader('expires',http_date(-3600 * 24 * 365))
                req._v_request.endHeaders()
            return response
        except Exception as e:  # describe an application error message
            print('============= TRACEBACK ================')
            sys.stderr.write(DATE().time() + '\n')
            sys.stderr.write(url + '\n')
            traceback.print_exc(file=sys.stderr)
            sys.stderr.write('%s\n' % e)
            print('============= END ================')
            raise
            req.error = e or "unspecified error"
            return self.doUnknown(req)

#    def doFlatfile(self, req, name):
#        '''
#        return flat file
#        BEWARE: assumes that the file won't change for a week
#        '''
#        print ("flat file name >>>>>:", name)
#        try:
#            kind = name.rsplit('.', 1)[1].lower()
#            mime = (kind == 'ico') and 'image/x-icon' or types_map.get('.'+kind) \
#                or 'text/plain'  # don't know why '.ico' is missing from types-map...
#            data = open(name, 'rb').read()
#            req._v_request.setHeader('content-type', mime)
#            # prevent browser from asking for image every page request
#            # assumes won't change for a week!
#            req._v_request.setHeader('expires',http_date(3600 * 24 * 7))
#            return data
#        except:
#            req.error = f"file {name} not found"
#            return doUnknown(req) 

    def doDefault(self, req):
        "give the default URL"
        return self.doMethod(req, req.HOME, 'view')

    def doUnknown(self, req):
        ""
        msg = req.error or "resource not found"
        # we generally don't want a fancy rendered error page here
        req._v_request.setResponseCode(404, msg)
        req._v_request.endHeaders()
        return ""
#        return msg

class dispatcherInit(object):
    ""

    def __init__(self):
        "system start-up"
#        print("dispatcher initialising....")
        # set up the application and the security checks
        self.app = App() # build the app classes
        self._sync = _thread.allocate_lock()
        self.acquire = self._sync.acquire
        self.release = self._sync.release
        self.locked = self._sync.locked

class Dispatcher(Url, dispatcherCode, dispatcherInit):
    "combine component classes"
    pass
