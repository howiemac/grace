"""
search - search facility - by hierarchical branch

- a mix-in class for Page klass
- gives a context sensitive search of self and all descendents only
- implemented by default as a page "search" tab
- see also def tagged() in tags.py, which provides a tag search:
  - implemented via the same "search" tag
  - uses the same search_results() listing defined here


Ian Howie Mackenzie March 2021
"""

from grace.lib import *
from grace.render import html


class search(object):

    # should search tab be made available?
    searchablekinds=("page",)

    def searchable(self):
        """does it make sense to allow search tab for this page?
           - for use in Page.get_pageoptions() in Page.py
           Returns the number of child pages matching searchablekinds.
        """
        return self.count(parent=self.uid,isin={"kind": self.searchablekinds})

    # display ("search" tab)
    @html
    def search_tab(self, req):
        "search"
        req.tab = "search"

    # results
    @html
    def search_results(self, req):
        "search results"
        req.tab = 'search'
        found=len(req.pages)
        # messages
        pagefrom = req.pagenext-req.pagesize+1
        if req.pages:
            if found == req.pagesize:
                req.found = f'showing results {pagefrom} to {req.pagenext} {req.matching} "{req.find}"'
            else:
                what="final " if pagefrom>1 else ""
                s= "s" if found > 1 else ""
                req.found = f'showing {what}{found} result{s} {req.matching} "{req.find}"'
        else:
            more = "more " if pagefrom>1 else ""
            req.notfound = f'no {more}results found {req.matching} "{req.find}"'

    # search (paged)
    #
    # note: title search is now combined with text search, so results are now properly sorted
    # todo? add a separate title-only search option, based on the commented out code below.
    #
    def search(self, req, limit=50):
        """text search - requires req.find
           - as called via Page_search_tab.evo
           - searches against names and text
           - no use of full-text indexing
           - scope depends on where search is called from
               - searches self and all of its descendents
               - descendents are matched via self.branchclause() which can be overridden
           - result order is as selected on the search form (req.order)
           - results are paged:
               - page-size is set by limit parameter above
               - search results are effectively unlimited in number
           - % or * can be used as wildcard within req.find search term
               - this will match any intervening text
           - " and " in req.find will be ignored
        """
        lim=page(req,limit) # lim = sql limit value, and req now has req.pagenext and req.pagesize (==limit)
        found=0
        heads=[]
        # sanitise the search term - ignore quotes, and allow * as wildcard
        term=req.find.upper().replace('"', '').replace("'", '').replace('*', '%')
        # order clause - as selected via Page_search_tab.evo
        order=f"`{req.order or 'uid'}` {'desc' if req.reverse else ''}"
        # get scope where clause: will search self and all descendents
        scope= self.branchclause()
        # search for matches..
        if term:
 #           # get name (title) matches first
 #           lineage=sql_list([i for i in self.lineage.split(".")])
 #           where=f'{scope} and name like "%{term}%"'
 #           heads.extend(self.list(where=where, orderby=order, limit=lim,_debug=False))
 #           found=len(heads)
 #           # now get any text matches
            #  get title or text matches
            if found < limit:
                # adjust lim for the matches already found
                lim=f"{req.pagenext - limit}, {limit - found}"
                # get text matches (brute force i.e. not "full-text")
#                where=f'{scope} and text like "%{term}%"'
                where=f'{scope} and (name like "%{term}%" or text like "%{term}%")'
                texts = self.list(where=where, orderby=order, limit=lim,_debug=False)
                if texts:
                    heads.extend(texts)
                    found = len(heads)
        # add the results to req for display
        req.page = 'search'
        req.pages = heads
        req.call = "search" # call search() when paging the results
        req.matching = "matching"
        return self.search_results(req)
#  search.permit="guest"#allow anybody in
