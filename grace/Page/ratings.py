"""
ratings / enable / disable - Page rating routines

mix-in class for Page klass

ratings values: (the higher the rating, the more valuable)
    3 : enabled great
    2 : enabled good
    1 : enabled OK (keep)
    0 : enabled ? (default)
   -1 : disabled great
   -2 : disabled good
   -3 : disabled OK
   -4 : disabled ?
   -5 : rejected / marked for deletion

Standard ratings interface: ratings can be incremented or decremented or disabled/enabled

Also covers scores (rudimentary implementation - no score setting - hence the default Config.show_scores is False).

Ian Howie Mackenzie Sept 2021
"""

from grace.lib import *
from grace.render import html

class ratings(object):

    ratedkinds=("page","image")
    scoredkinds=("page",)

    downratings=(-5,-5,-4,-3,-2,-5,0,1,2)
    upratings=(0,-3,-2,-1,-1,1,2,3,3)
    disratings=(0,0,1,2,3,-4,-3,-2,-1)

    def rating_index(self,rating=None):
        """
        convert rating to tuple (or list) index
        - rating should be in (-5,-4,-3,-2,-1,0,1,2,3)
        - result returned should be in range 0 through 8
        """
        return min(8,max(0,(rating if rating is not None else self.rating)+5))

    # non glyphicon version

    # access these via rating_symbol()
    rating_symbols=('&times;','?','&radic;','&diams;','&hearts;','?','&radic;','&diams;','&hearts;')

    def rating_symbol(self,rating=None):
        "give symbol for rating"
        r=self.rating_index(rating)
        return self.rating_symbols[r]

#   # glyphicon version NOT CURRENTLY USED - NEEDS ANOTHER ICON ADDED TO REPLACE THE FIRST 'heart'
#
#    # access these via rating_class()
#    rating_classes=('remove-sign','question-sign','ok-sign','heart','question-sign','ok-sign','heart','heart')
#
#    def rating_class(self,rating=None):
#        "give class for rating"
#        r=self.rating_index(rating)
#        return "glyphicon glyphicon-%s" % self.rating_classes[r]

    # utility

    def set_rating(self,rating):
        "sets self.rating to rating"
        self.rating=rating
        self.flush()

    # user interface

    def rating_return(self,req):
        """
        common return for all re-rating options (for overriding)
        - optional req.view gives a redirect function name
        """
        return req.redirect(self.url(req.view))

    def rate_up(self,req):
        "increase rating"
        self.rating=self.upratings[self.rating_index()]
        self.flush()
        return self.rating_return(req)

    def rate_down(self,req):
        "decrease rating"
        self.rating=self.downratings[self.rating_index()]
        self.flush()
        return self.rating_return(req)

    def toggle_disable(self,req):
        "disable / enable"
        self.rating=self.disratings[self.rating_index()]
        self.flush()
        return self.rating_return(req)

    def ratingclause(self,ratings):
        " generic rating where clause: ratings should be an iterable, or False"
#        ratings=list(self.rating_filter())
        if ratings:
            return f"rating in {sql_list(ratings)}"
        # no filter set, so ignore it
        return "true" # mysql-speak for "ignore me"

    ## Note: the following is in music app and could be made generic 
    ## - in which case:
    ##    -  it should have a "ratings" parameter, defaulting to None
    ##    -  ratingclause(ratings) should replace self.rating_filter() below...
    ##    -  self.rating_filter() should be passed (if required) when calling this method
    ## - this could be achieved more neatly by adding the optional rating filtering to get_children_by_kind() in Page.py
    #
    #def get_rating_filtered_children_by_kind(self, kind="", orderby='seq,uid'):
    #    "get all children of given (or own) kind where rating included in rating_filter set"
    #    return self.list(
    #        parent=self.uid, kind=kind or self.kind, where=f"rating in {sql_list(self.rating_filter())}", orderby=orderby)
    #
    ## end of Note

    # rating filter - used for filtering pages when listing / displaying them

    ### O/S NEEDS TO BE MADE MULTI-USER USING SESSION CACHE


    def toggle_rating_filter(self,req):
        """form handler for setting global rating-filter - requires req.rating
           - allows toggle-selection of any combination of ratings
          "toggles given rating in rating filter"
        """
        # get the existing filter
        newfilter = self.get_rating_filter()
#        print("oldfilter: ",newfilter)
        # toggle the rating
        newfilter ^= {safeint(req.rating)}
#        print("newfilter: ",newfilter)
        # store it
        self.set_rating_filter(newfilter)
        return self.rating_return(req)

    def set_rating_minimum(self,req):
        """form handler for setting global rating-filter - requires req.rating
           - allows everything above and equal to the selected rating
        """
        xref={-5:{-5,-4,-3,-2,-1,0,1,2,3},
              -4:{-4,-3,-2,-1,0,1,2,3},
              -3:{-3,-2,-1,0,1,2,3},
              -2:{-2,-1,0,1,2,3},
              -1:{-1,0,1,2,3},
               0:{0,1,2,3},
               1:{1,2,3},
               2:{2,3},
               3:{3}
               }
        newfilter = xref[safeint(req.rating)]
        self.set_rating_filter(newfilter)
        return self.rating_return(req)

    def get_rating_filter(self):
        "returns rating filter set"
        s=self.get(1).code
        return eval(s) if s else set()
    rating_filter=get_rating_filter

    def set_rating_filter(self,ratings):
        "stores rating filter in root's code column - must be a set or convertable to a set"
        root=self.get(1)
        root.code=str(ratings)
        root.flush()

