"""
tags - Page tag handling - interface with Tag klass

mix-in class for Page klass
"""

from urllib.parse import unquote_plus


from grace.lib import *
from grace.render import html
from grace.data import execute

class tags(object):

    def get_tags(self):
        "return a list of tag names for this page (if any)"
        names=[i.name for i in self.Tag.list(page = self.uid, orderby = 'name')]
        return names

    def get_display_tags(self):
        """return a list of tag names for this page (if any) for display purposes
        - avoids space stretching (justification) within tags, by using "&nbsp;"
        """
        names=[i.replace(" ","&nbsp;") for i in self.get_tags()]
        return names

    def get_recent_tags(self, limit = 30):
        "return a list of tag names recently used"
        return [i.name for i in self.Tag.list(orderby = "uid desc", limit = limit)]

    def get_all_tags(self):
        "return a list of all currently used tag names"
        names= [str(i.get('name')) for i in self.Tag.list(asObjects = False, what = "distinct name", orderby = "name")]
        return names

    def tagname_count(self):
        "return (inefficiently derived) total number of distinct tagnames"
        tot = len(self.Tag.list(asObjects = False, what = "distinct name"))
        return tot

    def add_tag(self,name):
        "add a new tag for self, if it doesn't already exist - returns new tag object"
        name=name.replace(",","") # commas are used to separate multiple tags, and so are not allowed in a tag
        name=name.replace('"',"'") # double quotes are used in the SQL queries, and so are not allowed in a tag
        if name.lower() not in (i.lower() for i in self.get_tags()):
            tob=self.Tag.new()
            tob.name=name
            tob.page=self.uid
            tob.flush()
            return tob
        return None

    def delete_tag(self,name):
        "delete named tag for self (page)"
        for t in self.Tag.list(page=self.uid,name=name):
            t.delete()


    # user interface

    @html
    def tags_tab(self,req):
        "tagging / untagging tag cloud display"
        req.tab = 'tags'

    def tag(self,req):
        "add tag of req.name for self"
        if req.name:
            name=unquote_plus(req.name).strip()
            self.add_tag(name)
        return req.redirect(self.url("tags_tab"))

    def untag(self,req):
        "remove tag of req.name from self"
        if req.name:
            name=unquote_plus(req.name)
            self.delete_tag(name)
        return req.redirect(self.url("tags_tab"))

    def delete_tags(self):
        """delete the tags associated with self
        """
        for t in self.Tag.list(page=self.uid):
            t.delete()
 
    # tag statistics (for entire database)
    @html
    def tag_stats(self,req):
        "displays tag usage by tagname"
        req.tagnames=self.execute(sql=f"select name, count(uid) as total from {self.Tag.table} group by name order by total desc, name")

    # generic SQL where clause used by listings below

    #   NOTE (IHM Mar 2021) branchclause() should perhaps be in Page.py,
    #   and used in similar composite listings to those below,
    #   along with ratingclause() from rating.py
    #   potentially replacing many of the current "utility method" based
    #   listings in Page.py with more power and flexibility.....

    def branchclause(self):
        "returns sql WHERE clause for self and all descendents"
        scope=f'((pages.uid={self.uid}) or (instr(lineage,".{self.uid}.") > 0))'
        return scope

   # query for tag cloud display

    def get_sized_tagnames(self, ratings=None):
        """return a complete list of (tagname,textsize) tuples, where textsize indicates usage
           - optional ratings parameter allows a ratings filter to be passed (list of ratings to include)
        """
        sql=f"""select tags.name, count(pages.uid) as subtotal
            from {self.table} as pages
            inner join {self.Tag.table} as tags
            on page = pages.uid
            where {self.branchclause()}
            and {self.ratingclause(ratings)}
            group by tags.name
            order by tags.name
        """
        #print(sql)
        data=[]
        tagcounts=self.list(sql=sql,asObjects=False,_debug=False)
        total=sum(t["subtotal"] for t in tagcounts)
        for t in tagcounts:
            z=total//t["subtotal"]
            textsize = "huge" if z<=8 else \
                 "big"  if z<=32 else \
                 "norm" if z<=256 else \
                 "wee"  if z<=4096 else \
                 "tiny"
            data.append((t["name"],textsize))
        return data


    # tag selection for tag-based search

    def selecttag(self,req):
        """ add to (or remove from) a tag selection list for viewing
            - req.tag is an optional string of comma-separated tag-names
            - req.addtag is an optional tag to add
            - if req.addtag is already in taglist it is negated by prepending a ~
            - if negated addtag is already in tags, req.addtag is removed from taglist
            returns search_tab page with update tabs as req.tag
        """
        sep="," # separator
        tags=[]
        if req.tag:
            tags=req.tag.split(sep)
        if req.addtag:
            if req.addtag in tags: # negate it
                tags[tags.index(req.addtag)]="~"+req.addtag
            elif ("~"+req.addtag) in tags: # remove it
                tags.remove("~"+req.addtag)
#                tags[tags.index("~"+req.addtag)]=req.addtag
            else: # add the tag
                tags.append(req.addtag)
#        if req.deltag and req.deltag in tags:
#            tags.remove(req.deltag)
        req.tag=sep.join(tags)
        return req.redirect(self.url("search_tab",tag=req.tag))

    # tag-based search results

    def tag_results(self, req):
        ""
        req.call = req.matching = "tagged" # for paging (call) and "found"  message (matching)
        return self.search_results(req)

    def tagged(self,req,limit=50):
        """tag search - requires req.find
           - req.find can be a single tag, or comma-separated multiple tags to be combined
           - any tag prefixed with ~ is interpreted as a tag to exclude (boolean NOT)
           - returns a listing of all child pages matching req.find
           - as called via Page_search_tags_form.evo
           - scope depends on where search is called from:
               - searches self and all siblings, and their descendents
           - order: result order is as selected on the search form (req.order)
           - results are paged:
               - page-size is set by limit parameter above
               - search results are effectively unlimited in number
        """
        tag=req.find
        # order clause - as selected via Page_search_tags_form.evo
        order=f"`{req.order or 'uid'}` {'desc' if req.reverse else ''}"
        if tags=="UNTAGGED":
            return self.untagged(req,limit)
        req.pages=[]
        if tag:
            lim=page(req,limit)
            req.pages=self.children_by_tag(tag=tag,order=order,limit=lim)
        return self.tag_results(req)

    def untagged(self,req,limit=50):
        " returns a listing of all child pages with no tag "
        lim=page(req,limit)
        req.pages=self.children_untagged(order=req.order,limit=lim)
        return self.tag_results(req)

    # query for hierarchical tag data

    def children_by_tag(self,tag="",order="uid",limit="",ratings=None,above=None,below=None):
        """ return a list of all child page objects with given tag
        - depends on self.branchclause() and self.ratingclause(), which be overridden to vary the scope
        - "tag" is a string with a single tag  or comma-separated multiple tags to be combined
          - tags prefixed with ~ are interpreted as tags to exclude (boolean NOT)
        - "ratings" parameter gives an interable of ratings to include
          - the standard self.ratingclause() takes effect only if a True ratings iterable is provided
        - "above" and/or "below" - if set - limits the results to uids above and/or below this value
        NOTE - this could also usefully include optional filtering by page kind 
        """
        if (not tag) or (tag=="UNTAGGED"):
            return self.children_untagged(order=order,limit=limit,above=above,below=below)
        # allow for multiple tags and exclusion tags
        tags=tag.split(",")
        andclause=f'uid %sin (select distinct page from {self.Tag.table} where name="%s")'
        andclauses=[(andclause % ("not " if i[0]=="~" else "",i.lstrip("~"))) for i in tags]
        tagclause=" and ".join(andclauses)
        # allow for "above and "below" parameters
        aboveclause= f"and pages.uid>{above}" if above else ""
        belowclause= f"and pages.uid<{below}" if below else ""
        # limit, if any
        limitclause= f"limit {limit}" if limit else ""
        # put it all together
        sql=f"""select * from {self.table}
               where {self.branchclause()}
               and {self.ratingclause(ratings=ratings)}
               {aboveclause}
               {belowclause}
               and {tagclause}
               order by {order}
               {limitclause}
            """
#        print(">>>>>>>>>>>>>", sql)
        return self.list(sql=sql,_debug=False)

    def children_untagged(self,order="uid",limit="",ratings=None,above=None,below=None):
        """ returns a list of all child pages with no tag
        - depends on self.branchclause() and self.ratingclause(), which be overridden to vary the scope
        - uses the rating filter, if set, via self.ratingclause()
        - "ratings" parameter gives an interable of ratings to include
          - the standard self.ratingclause() takes effect only if a True ratings iterable is provided
        - "above" and/or "below" - if set - limits the results to uids above and/or below this value
        NOTE - this could also usefully include optional filtering by page kind
        """
        # allow for "above and "below" parameters
        aboveclause= f"and pages.uid>{above}" if above else ""
        belowclause= f"and pages.uid<{below}" if below else ""
        # limit, if any
        limitclause= f"limit {limit}" if limit else ""
        # put it all together
        sql=f"""select pages.* from {self.table}
               left join {self.Tag.table}
               on tags.page=pages.uid
               where {self.branchclause()}
               and {self.ratingclause(ratings=ratings)}
               and tags.page is NULL
               {aboveclause}
               {belowclause}
               order by pages.{order}
               {limitclause}
            """
    #    print(">>>>>>>>>>>>>", sql)
        return self.list(sql=sql,_debug=False)

    # tag utilities

    @classmethod
    def remove_duplicate_tags(self,req):
      """ removes all duplicate tags
      - returns tags_tab with message
      - note that duplicate tags should never arise when using only the routines in this module
      """
      self=self.get(1) # make this global
      xtags=self.Tag.count()
      self._remove_duplicate_tags()
      tags=xtags-self.Tag.count()
      # return tags tab with message
      req.message=f'{tags} duplicate tags have been removed'
      return self.tags_tab(req)

    def _remove_duplicate_tags(self):
      """ removes all duplicate tags
      """
      sql= f"select uid,page,name from (select count(uid) as occ, uid, page, name from {self.Tag.table} group by page,name) as custom where occ>1"
      for t in self.Tag.list(sql=sql):
        #res=self.Tag.list(where = f'name="{t.name}" and page="{t.page}" and uid!={t.uid}')
        #for i in res:
        #  print("uid:",i.uid," name:",i.name," page:",i.page)
        execute(f'delete from {self.Tag.table} where name="{t.name}" and page="{t.page}" and uid!={t.uid}')

    @classmethod
    def rename_tags(self,req):
      """ renames all tags named req.old to req.new
      - requires req.old and req.new
      - removes any duplicates created by this renaming
      - returns tags_tab with message
      """
      self= self.get(1) # make this global
      self._rename_tags(req.old,req.new)
      # eliminate any duplicates created by this
      self._remove_duplicate_tags()
      # return tags tab with message
      req.message=f'all tags named "{req.old}" have been renamed "{req.new}"'
      return self.tags_tab(req)

    def _rename_tags(self,oldname,newname):
      """ renames all tags named oldname to newname
      """
      sql= f'update {self.Tag.table} set name="{newname}" where name="{oldname}"'
      execute(sql=sql)
