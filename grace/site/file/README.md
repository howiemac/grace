This folder contains flat-file resources associated with the Page klass.

The files are organised by page uid, using nesting to (hopefully) prevent any directory from becoming too large.

Seg: /001/002/1002345.jpg refers to a jpeg image file associated with page uid 1002345

If more than 999,999,999 uids have to catered for, this scheme should be extended...

(IHM July 2020)