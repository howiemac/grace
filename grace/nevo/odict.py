""" An overlay dict lets you update a dict reversably.

    We use a list of dicts and search backwards looking for the most recent instance of that key

    In addition to the usual dict methods we add:

    .add    - add a context
    .remove - remove a context

"""
from collections import ChainMap, defaultdict


class Odict(ChainMap):
    "A dictionary with overlaid context"
    def __init__(self, *maps, strict=False):
        "make sure the last map is a defaultdict"
        maps = list(maps)
        if strict:
            maps.append({})
        else:
            maps.append(defaultdict(lambda: ''))
        ChainMap.__init__(self, *maps)
    
    def add(self, d=None):
        "add a context"
        d = d or {}
        self.maps.insert(0, d)

    def remove(self):
        "remove the topmost context"
        if len(self.maps) > 1:
            return self.maps.pop(0)
        else:
            raise IndexError

