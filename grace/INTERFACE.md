# Interface (API)

## server

- req: an object-wrapped dictionary of request parameters and their values, with useful functions
- response: a string of text or data, as requested

## data

(current)

- flush : commits data to persistend db
- get : returns a single object from db
- list : returns a list of objects (or dictionaries) per criteria, from db
- execute : SQL string execution
  - parameters: (as per evoke get() and list())

(proposed)

- put : commits data to persistent data store
- get :
    - get(uid) returns a single object
    - get(<conditions>) returns a list of objects or dictionaries
    - get(sql, sqlargs) returns a list of objects or dictionaries