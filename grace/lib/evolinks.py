"""
render evo links - [uid] or [url] or [uid caption] or [url caption]

This is for over-ride of the Markdown package - used in TEXT.py - so that it can use our style of links.

Note that markdown link-style [caption](url) can also be used.

"""

from markdown import Extension
from xml.etree import ElementTree as etree
#from markdown.util import etree
from markdown.inlinepatterns import Pattern


def safeint(s):
  ""
  try:
    return int(s)
  except:
    return 0


class EvoLinkExtension(Extension):

  def __init__(self, *args, **kwargs):
    ""
    super(EvoLinkExtension, self).__init__(*args, **kwargs)

#  def extendMarkdown(self, md, md_globals):
  def extendMarkdown(self, md):
    ""
    self.md = md

#    EVOLINK_RE = r'\[(.*?)( .*?)?\]'
    EVOLINK_RE = r'\[([^^]*?)( .*?)?\]' # evo link style - allows [^whatever] footnote to be passed through to markdown

    evolinkPattern = EvoLinks(EVOLINK_RE, self.getConfigs())
    evolinkPattern.md = md
#    md.inlinePatterns.add('evolink', evolinkPattern, "<not_strong") # .add is deprected from markdown 3.0, and removed in 3.4
    md.inlinePatterns.register(evolinkPattern, 'evolink', 20) # now using .register instead of .add


class EvoLinks(Pattern):
  ""
  def __init__(self, pattern, config):
    ""
    super(EvoLinks, self).__init__(pattern)
    self.config = config

  def handleMatch(self, m):
    "return html for match"
    # before = m.group(1)
    target = m.group(2).strip()
    caption = (m.group(3) or '').strip()
    # after = m.group(4)
    cls=''
    extra = {}
    # is target a valid page uid?
    uid=safeint(target)
    if uid:
      try:
        page = self.md.req.HOME.Page.get(uid)
#        page = self.md.req.HOME.get(uid) #THIS IS ENOUGH  SURELY _ NEEDS TESTED
        name = page.name
        target = page.url()
      except:
        extra = {'class': 'broken'}
        name = ""
        target = ""
      caption = caption or name or target
    else:
      # we assume an external url
      caption = caption or target
      extra = {'rel': 'nofollow'}
    a = etree.Element('a')
    a.text = caption
    a.set('href', target)
    for k, v in list(extra.items()):
      a.set(k, v)
    return a

  def _getMeta(self):
    """ Return meta data or config data. """
    # TODO - needed?
