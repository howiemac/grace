"""
GRACE library interface

makes library types and functions available via grace.lib.

Ian Howie Mackenzie (April 2017, March 2020)
"""

# GRACE types
from .INT import INT, SMALLINT, TINYINT # note SMALLINT and TINYINT are DEPRECATED in GRACE
from .FLOAT import FLOAT
from .STR import STR, TAG, CHAR # note CHAR is DEPRECATED in GRACE
from .DATE import DATE
from .TEXT import TEXT
from .REL import REL
from .FLAG import FLAG
from .BLOB import BLOB
# GRACE library functions
from .library import sql_list, safeint, http_date, page, prev, next, url_safe, elapsed, delf, unwrap, wrap, replace
from .email import email
from .error import Error
from .permit import Permit
