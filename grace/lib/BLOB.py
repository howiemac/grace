"""
This module implements the BLOB class.

Ian Howie Mackenzie (July 2007 and March 2020)
"""

class BLOB(str):
    """
  simple blob handling
  """

    def __new__(self, data=""):
        self.data = data

    def sql(self, quoted=True):
        """ gives sql string format, including quotes 
    """
        # THIS NEEDS SORTED FOR SQLite....
        if quoted:
#            return '"%s"' % MySQLdb.escape_string(str(self.data))
            return '"%s"' % str(self.data)
        else:
#            return '%s' % MySQLdb.escape_string(str(self.data))
            return '%s' % str(self.data)

    def __str__(self):
        if isinstance(self.data, type('')):
            return self.data
        else:  #must be array
            return self.data.tostring()

    def __repr__(self):
        return repr(self.data)

    _v_default = ""
    _v_sqlite_type = _v_mysql_type = "blob"
