# -*- coding: utf-8 -*-
"""
default config file for grace

configuration value here act as defaults for ALL apps,
so NO PARAMETER SHOULD BE REMOVED
"""

from .data.schema import *


# for information only:
# ---------------------

# the following is set in __init__.py and should NOT be overridden:
# grace_filepath= ...

# the following are set in serve/app.py and should NOT be overridden:
# grace_version= ...
# copyright= ...  this message is shown on the console log on startup
# app_filepath= ...


# defaults:
# ---------

# the following may be overridden in the app's config.py or config_site.py
site_filepath="site/" # the location of all flat file resources for the webserver
#domains = ["127.0.0.1","grace"]  # eg ['versere.com','www.versere.com']
domains=["grace","127.0.0.1","localhost"]
domain = 'grace'  #if not provided,  this is set to domains[0] by serve/app.py
database = ''  #this will default to <app name>.db (no path - ie in the current working directory)
port = '9000'
meta_description = "grace web application engine"
meta_keywords = "grace, web, application, engine, server, python, mysql"
urlpath = ''  # prefix to url (if any) - must have a preceding "/", or else be ""
urlhost = ''  # for external use (not recommended - see serve/url.py). Defaults to 'http://'+domain (assumes port 80 - see serve/app.py)
default_class="Page" #lets us do page urls as domain/123 (or, if urlpath, domain/urlpath/123), rather than domain/page/123 (or domain/urlpath/page/123)
mailto = 'mail@localhost'  #contact email address

# include config.py files from class folders
# O/S THIS SHOULD BE DONE AUTOMATICALLY BY serve/app.py
from .Page.config import *
from .Tag.config import *
