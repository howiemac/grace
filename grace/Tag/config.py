"""
config file for Tag

"""

from grace.data.schema import *

#the following data schema may be subclassed or overridden in an app's config.py

class Tag(Schema):
    table = 'tags'
    name = TAG, KEY
    page = INT, KEY
    

